"""
Creates a list of every substance + compartment with their carbon content. Used for creating a simapro method.
"""

import os
import csv

from ecoinvent_carbon_balance.vars import RESULTS_FOLDER, CARBON_CONTENT_FROM_SIMAPRO_FOLDER, \
    SUBSTANCES_CARBON_CONTENT_FILEPATH

simapro_substances_filepath = os.path.join(CARBON_CONTENT_FROM_SIMAPRO_FOLDER, 'ecoinvent_substances.csv')
substances_names_conversion_filepath = os.path.join(RESULTS_FOLDER, 'substances_names_conversion.csv')
result_filepath = os.path.join(RESULTS_FOLDER, 'substances_carbon_content_by_compartment.csv')

# Reading substances from Ecoinvent ecospolds
with open(SUBSTANCES_CARBON_CONTENT_FILEPATH, 'r') as file:
    reader = csv.DictReader(file, delimiter=';')
    substances = list(reader)

# Reading substances obtained from Simapro
simapro_substances = list()
with open(simapro_substances_filepath, 'r') as file:
    reader = csv.reader(file, delimiter=';')

    firstrow = True
    for row in reader:
        # Skipping first row
        if firstrow:
            firstrow = False
            continue

        simapro_substances.append([row[0], row[1], row[2], row[3]])

# Reading substances names conversion
simapro_to_ecoinvent = dict()  # Dict containing ecoinvent names as keys and simapro names as values
with open(substances_names_conversion_filepath, 'r') as file:
    reader = csv.reader(file, delimiter=';')

    firstrow = True
    for row in reader:
        # Skipping first row
        if firstrow:
            firstrow = False
            continue

        if row[1]:
            simapro_to_ecoinvent[row[0]] = row[1]

# Creating the reverted dict
ecoinvent_to_simapro = {v: k for k, v in simapro_to_ecoinvent.items()}

# Looping on each substances
for sub in substances:
    if sub['simapro name'] == '':
        if sub['name'] in [s[1] for s in simapro_substances]:
            sub['simapro name'] = sub['name']
        elif sub['name'] in ecoinvent_to_simapro:
            sub['simapro name'] = ecoinvent_to_simapro[sub['name']]
        else:
            sub['simapro name'] = ''
    elif sub['name'] == '':
        if sub['simapro name'] in simapro_to_ecoinvent:
            sub['name'] = simapro_to_ecoinvent[sub['sumapro name']]

# Creating a list of substances with their compartments and their carbon content
substances_final = list()
simapro_names_cc = {s['simapro name']: s['carbon content'] for s in substances}
for sub in simapro_substances:
    if sub[1] in simapro_names_cc:
        sub.append(float(simapro_names_cc[sub[1]]))
        substances_final.append(sub)
    if 'Water' in sub[1] and sub[0] != 'Air':
        sub.append(0.001148657)
        substances_final.append(sub)

# Exporting result
with open(result_filepath, 'w', newline='') as file:
    writer = csv.writer(file, delimiter=';')

    writer.writerow(['compartment', 'name', 'unit', 'cas', 'carbon_content'])
    writer.writerows(substances_final)

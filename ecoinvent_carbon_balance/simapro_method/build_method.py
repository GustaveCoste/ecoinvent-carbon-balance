"""
Building a simapro method from carbon content data.
"""

import os
import csv
import shutil

from ecoinvent_carbon_balance.vars import BASE_FOLDER, RESULTS_FOLDER, METHOD_TEMPLATE_FILEPATH

method_filepath = os.path.join(BASE_FOLDER, 'carbon_balance.csv')
substances_filepath = os.path.join(RESULTS_FOLDER, 'substances_carbon_content_by_compartment.csv')

# Reading the substances
with open(substances_filepath, 'r') as file:
    reader = csv.DictReader(file, delimiter=';')
    substances = list(reader)

# Building the method
shutil.copyfile(METHOD_TEMPLATE_FILEPATH, method_filepath)
with open(method_filepath, 'a') as method_file:
    # Appening substances with non null or empty values
    for substance in substances:
        if float(substance['carbon_content']) != 0:

            # Reverting the sign of the value
            if substance['compartment'] != 'Raw':
                substance['carbon_content'] = '-' + substance['carbon_content']

            method_file.write('\n' + ';'.join((substance['compartment'],
                                               '(unspecified)',
                                               substance['name'],
                                               substance['cas'] or '',
                                               str(substance['carbon_content']),
                                               substance['unit'])))

"""
Extracts the corrected values from the substances comparison list (comparison of values obtained via ecoinvent ecospolds
and via old method with Simapro) and puts it in a final file.
The comparison file (substances_comparison.csv) computed by compare_substances_with_other_method.py must be edited by
adding two new columns (or the right) containing corrected value for carbon contents and origin of the value.
"""

import os
import csv

from ecoinvent_carbon_balance.vars import RESULTS_FOLDER, SUBSTANCES_CARBON_CONTENT_FILEPATH

comparison_filepath = os.path.join(RESULTS_FOLDER, 'substances_comparison_edit.csv')

# Reading substances comparisons
substances_comparisons = list()
with open(comparison_filepath, 'r') as file:
    reader = csv.reader(file, delimiter=';')

    firstrow = True
    for row in reader:
        # Skipping first row
        if firstrow:
            firstrow = False
            continue

        substances_comparisons.append(row)

# Writting the final file
with open(SUBSTANCES_CARBON_CONTENT_FILEPATH, 'w', newline='') as file:
    writer = csv.writer(file, delimiter=';')
    writer.writerow(['name', 'simapro name', 'carbon content'])

    for substance in substances_comparisons:
        writer.writerow([substance[0], substance[1], substance[5]])

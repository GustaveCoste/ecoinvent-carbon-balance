"""
Script to update the substances list with corrected values for conflictual substances.
The conflict file (conflicts.csv) computed by extract_substances_list.py must be edited by adding a new column
(to the right) containing corrected value for carbon contents.
"""

import os
import csv

from ecoinvent_carbon_balance.vars import RESULTS_FOLDER

substances_filepath = os.path.join(RESULTS_FOLDER, 'substances.csv')
conflicts_filepath = os.path.join(RESULTS_FOLDER, 'conflicts_edit.csv')
updated_substances_filepath = os.path.join(RESULTS_FOLDER, 'updated_substances.csv')

# Reading substances
substances = dict()
with open(substances_filepath, 'r') as file:
    reader = csv.reader(file, delimiter=';')

    firstrow = True
    for row in reader:
        # Skipping first row
        if firstrow:
            firstrow = False
            continue

        substances[row[0]] = row[1]

# Reading corrected values for conflictual substances and updating substances values
with open(conflicts_filepath, 'r') as file:
    reader = csv.reader(file, delimiter=';')

    firstrow = True
    for row in reader:
        # Skipping first row
        if firstrow:
            firstrow = False
            continue

        substances[row[0]] = row[7]

# Writing updated substances
with open(updated_substances_filepath, 'w', newline='') as file:
    writer = csv.writer(file, delimiter=';')
    writer.writerow(['substance', 'carbon content'])

    for substance in substances.items():
        writer.writerow([substance[0], substance[1]])

""""
Compares the substances obtained from Ecoinvent ecospolds with the ones obtained
with another method (from Simapro exports).
"""

import os
import csv

from ecoinvent_carbon_balance.vars import RESULTS_FOLDER, CARBON_CONTENT_FROM_SIMAPRO_FOLDER

ecoinvent_substances_filepath = os.path.join(RESULTS_FOLDER, 'updated_substances.csv')
simapro_substances_filepath = os.path.join(CARBON_CONTENT_FROM_SIMAPRO_FOLDER, 'ecoinvent_substances_with_cc.csv')
substances_names_conversion_filepath = os.path.join(RESULTS_FOLDER, 'substances_names_conversion.csv')
comparison_filepath = os.path.join(RESULTS_FOLDER, 'substances_comparison.csv')

# Reading substances from Ecoinvent ecospolds
ecoinvent_substances = dict()
with open(ecoinvent_substances_filepath, 'r') as file:
    reader = csv.reader(file, delimiter=';')

    firstrow = True
    for row in reader:
        # Skipping first row
        if firstrow:
            firstrow = False
            continue

        ecoinvent_substances[row[0]] = float(row[1])

# Reading substances obtained from Simapro
simapro_substances = dict()
with open(simapro_substances_filepath, 'r') as file:
    reader = csv.reader(file, delimiter=';')

    firstrow = True
    for row in reader:
        # Skipping first row
        if firstrow:
            firstrow = False
            continue

        simapro_substances[row[1]] = float(row[7])

# Reading substances names conversion
simapro_to_ecoinvent = dict()  # Dict containing ecoinvent names as keys and simapro names as values
with open(substances_names_conversion_filepath, 'r') as file:
    reader = csv.reader(file, delimiter=';')

    firstrow = True
    for row in reader:
        # Skipping first row
        if firstrow:
            firstrow = False
            continue

        if row[1]:
            simapro_to_ecoinvent[row[0]] = row[1]

# Creating the reverted dict
ecoinvent_to_simapro = {v: k for k, v in simapro_to_ecoinvent.items()}

# Filtering out substances without carbon content
simapro_substances = {k: v for k, v in simapro_substances.items() if v != 0}

# Merging results
substances = dict()
for sub in ecoinvent_substances.items():
    substances[sub[0]] = {'ecoinvent': sub[1], 'simapro': None}

    if sub[0] in simapro_to_ecoinvent:
        substances[sub[0]]['simapro_name'] = simapro_to_ecoinvent[sub[0]]
    else:
        substances[sub[0]]['simapro_name'] = None

for sub in simapro_substances.items():
    # Substances names match
    if sub[0] in substances:
        substances[sub[0]]['simapro'] = sub[1]

    # Substances names doesn't match but just need conversion
    else:
        try:
            assert simapro_to_ecoinvent[sub[0]] in substances
            for matching_sub in [s for s in substances.items() if s[0] == simapro_to_ecoinvent[sub[0]]]:
                matching_sub[1]['simapro'] = sub[1]
                matching_sub[1]['simapro_name'] = sub[0]

        # The substances doesn't exist in the Ecoinvent substances list
        except KeyError:
            substances[sub[0]] = {'ecoinvent': None, 'simapro': sub[1], 'simapro_name': None}

# Computing difference between carbon contents
for sub in substances.items():
    if sub[1]['ecoinvent'] is not None and sub[1]['simapro'] is not None:
        sub[1]['difference'] = abs(sub[1]['ecoinvent'] - sub[1]['simapro'])
    else:
        sub[1]['difference'] = None

union = {k: v for k, v in substances.items()
         if v['ecoinvent'] is not None
         and v['simapro'] is not None}

ecoinvent_only = {k: v for k, v in substances.items()
                  if v['ecoinvent'] is not None
                  and v['simapro'] is None}

simapro_only = {k: v for k, v in substances.items()
                if v['ecoinvent'] is None
                and v['simapro'] is not None}

diff_sup_1_prct = {k: v for k, v in substances.items() if (v['difference'] or 0) >= 0.01}

# Exporting results
with open(comparison_filepath, 'w', newline='') as file:
    writer = csv.writer(file, delimiter=';')

    writer.writerow(['name', 'simapro name (if different)', 'ecoinvent', 'simapro', 'difference'])
    for sub in substances.items():
        writer.writerow([sub[0], sub[1]['simapro_name'], sub[1]['ecoinvent'], sub[1]['simapro'], sub[1]['difference']])

"""
Script to extract a list of every substances with their carbon content from Ecoinvent ecospolds
"""

import os
import csv
import math
import statistics

import untangle
import tqdm

from ecoinvent_carbon_balance.vars import ECOINVENT_IO_FOLDER, RESULTS_FOLDER
from ecoinvent_carbon_balance.lib import get_flow_carbon_content

substances_filepath = os.path.join(RESULTS_FOLDER, 'substances.csv')
conflicts_filepath = os.path.join(RESULTS_FOLDER, 'conflicts.csv')

substances = dict()
conflicts = dict()

for filename in tqdm.tqdm(os.listdir(ECOINVENT_IO_FOLDER)):
    filepath = os.path.join(ECOINVENT_IO_FOLDER, filename)

    process = untangle.parse(filepath)

    try:
        process = process.ecoSpold.childActivityDataset
    except AttributeError:
        process = process.ecoSpold.activityDataset

    if hasattr(process.flowData, 'elementaryExchange'):
        if type(process.flowData.elementaryExchange) == list:
            elementary_exchanges = process.flowData.elementaryExchange
        else:
            elementary_exchanges = [process.flowData.elementaryExchange, ]
    else:
        elementary_exchanges = []

    for exchange in elementary_exchanges:
        substance_name = exchange.name.cdata
        substance_carbon_content = get_flow_carbon_content(exchange)

        if not substance_carbon_content:
            continue

        # If the carbon content is not the same than the one(s) previously inserted for this substance,
        # collects the error
        if substance_name in substances:
            if substances[substance_name] != substance_carbon_content:
                if substance_name in conflicts:
                    conflicts[substance_name].add(substance_carbon_content)
                else:
                    conflicts[substance_name] = {substances[substance_name], substance_carbon_content}
        else:
            substances[substance_name] = substance_carbon_content

# Writing substance list
with open(substances_filepath, 'w', newline='') as file:
    writer = csv.writer(file, delimiter=';')
    writer.writerow(['substance', 'carbon content'])

    for substance in substances.items():
        writer.writerow([substance[0], substance[1]])

# Writing conflicts list
with open(conflicts_filepath, 'w', newline='') as file:
    writer = csv.writer(file, delimiter=';')
    writer.writerow(['substance', 'carbon content values', 'mean', 'std dev', 'max diff', 'max', 'min', ])

    for conflict in conflicts.items():
        writer.writerow([conflict[0],
                         conflict[1],
                         statistics.mean(conflict[1]),
                         statistics.stdev(conflict[1]),
                         math.fabs(max(conflict[1]) - min(conflict[1])),
                         max(conflict[1]),
                         min(conflict[1])
                         ])

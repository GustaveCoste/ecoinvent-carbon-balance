import csv

from ecoinvent_carbon_balance.vars import SUBSTANCES_CARBON_CONTENT_FILEPATH


def get_substances_carbon_content_dict():
    """
    Reads the substances carbon content dictionnary

    Returns:
        dict: Dict contaning every substances carbon content (name: carbon_content)
    """

    with open(SUBSTANCES_CARBON_CONTENT_FILEPATH, 'r') as file:
        reader = csv.reader(file, delimiter=';')

        substances_carbon_content = dict()

        first_row = True
        for row in reader:
            if first_row:
                first_row = False
                continue

            substances_carbon_content[row[0]] = float(row[2])

    return substances_carbon_content


def get_flow_carbon_content(flow):
    """
    Gets the carbon content of a flow in kgC/unit of flow

    Args
        flow: Flow as an element of an Ecoinvent ecospold file

    Returns:
        float: carbon content of the flow
    """

    # If the flow has no properties, returns 0
    if not hasattr(flow, 'property'):
        return 0

    # If only one property, puts it in a list
    if type(flow.property) == list:
        properties = flow.property
    else:
        properties = [flow.property, ]

    # If carbon allocation (carbon mass of one unit of the flow) is already calculated, returns it
    carbon_allocation_lst = [prop for prop in properties if prop.name.cdata == 'carbon allocation']
    if carbon_allocation_lst:
        return float(carbon_allocation_lst[0]['amount'])

    # If not, calculates it from dry mass and carbon content
    carbon_content_list = [prop for prop in properties if prop.name.cdata == 'carbon content']
    carbon_content_list_fossil = [prop for prop in properties if prop.name.cdata == 'carbon content, fossil']
    carbon_content_list_non_fossil = [prop for prop in properties if
                                      prop.name.cdata == 'carbon content, non-fossil']

    if carbon_content_list:
        carbon_content = float(carbon_content_list[0]['amount'])
    elif carbon_content_list_fossil != [] and carbon_content_list_non_fossil != []:
        carbon_content = float(carbon_content_list_fossil[0]['amount']) + \
                         float(carbon_content_list_non_fossil[0]['amount'])
    else:
        carbon_content = 0

    dry_mass_list = [prop for prop in properties if prop.name.cdata == 'dry mass']
    wet_mass_list = [prop for prop in properties if prop.name.cdata == 'wet mass']
    if carbon_content != 0 and dry_mass_list != [] and wet_mass_list != []:
        dry_mass = float(dry_mass_list[0]['amount'])
        wet_mass = float(wet_mass_list[0]['amount'])
        carbon_content = carbon_content * (wet_mass / dry_mass)

    return carbon_content


def get_tech_flow_carbon_mass(flow):
    """
    Gets the carbon mass of a flow from/to technosphere

    Args:
        flow: Flow as an element of an Ecoinvent ecospold file

    Returns:
        float: carbon mass of the flow
    """
    flow_amount = flow['amount']

    if flow_amount == 0:
        return 0

    # If the flow has no properties, returns None
    if not hasattr(flow, 'property'):
        return 0

    # If only one property, puts it in a list
    if type(flow.property) == list:
        properties = flow.property
    else:
        properties = [flow.property, ]

    # If carbon allocation (carbon mass of one unit of the flow) is already calculated, returns the carbon mass
    carbon_allocation_lst = [prop for prop in properties if prop.name.cdata == 'carbon allocation']
    if carbon_allocation_lst:
        return float(carbon_allocation_lst[0]['amount']) * float(flow['amount'])

    # If not, calculates it from dry mass and carbon content
    dry_mass_list = [prop for prop in properties if prop.name.cdata == 'dry mass']
    carbon_content_list = [prop for prop in properties if prop.name.cdata == 'carbon content']
    carbon_content_list_fossil = [prop for prop in properties if prop.name.cdata == 'carbon content, fossil']
    carbon_content_list_non_fossil = [prop for prop in properties if prop.name.cdata == 'carbon content, non-fossil']

    if carbon_content_list:
        carbon_content = float(carbon_content_list[0]['amount'])
    elif carbon_content_list_fossil != [] and carbon_content_list_non_fossil != []:
        carbon_content = float(carbon_content_list_fossil[0]['amount']) + \
                         float(carbon_content_list_non_fossil[0]['amount'])
    else:
        return 0

    if dry_mass_list:
        dry_mass = float(dry_mass_list[0]['amount'])
    elif flow.unitName.cdata == 'kg':
        dry_mass = float(flow['amount'])
    else:
        return 0

    return dry_mass * carbon_content

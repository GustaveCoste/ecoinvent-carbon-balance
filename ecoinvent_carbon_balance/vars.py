import os

BASE_FOLDER = r"C:\Users\costegus\Documents\Ecoinvent carbon balance"

SYSTEM_MODEL = 'apos'
# SYSTEM_MODEL = 'cutoff'
# SYSTEM_MODEL = 'consequential'

VERSION = '3.4'
# VERSION = '3.5'

FILES_FOLDER = os.path.join(BASE_FOLDER, 'files')
DATA_FOLDER = os.path.join(r"C:\Ecoinvent data", SYSTEM_MODEL)
RESULTS_FOLDER = os.path.join(BASE_FOLDER, 'results', VERSION)
GRAPHS_FOLDER = os.path.join(BASE_FOLDER, '.graphs', SYSTEM_MODEL)

ECOINVENT_IO_FOLDER = os.path.join(DATA_FOLDER, 'ecoinvent {}_{}_ecoSpold02'.format(VERSION, SYSTEM_MODEL), 'datasets')
ECOINVENT_LCI_FOLDER = os.path.join(DATA_FOLDER, 'ecoinvent {}_{}_lci_ecoSpold02'.format(VERSION, SYSTEM_MODEL), 'datasets')

SUBSTANCES_CARBON_CONTENT_FILEPATH = os.path.join(BASE_FOLDER, 'results', VERSION, 'substances_carbon_content.csv')

CARBON_CONTENT_FROM_SIMAPRO_FOLDER = r"C:\Users\costegus\Documents\Calculateur Carbone\.data"

METHOD_TEMPLATE_FILEPATH = os.path.join(FILES_FOLDER, 'method_template.csv')

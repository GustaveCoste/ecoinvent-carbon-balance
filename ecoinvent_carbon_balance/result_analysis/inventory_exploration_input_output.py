"""
Script for data exploration (with debbuger)
"""

import os

import untangle
import tqdm

from ecoinvent_carbon_balance.vars import ECOINVENT_IO_FOLDER, RESULTS_FOLDER, SYSTEM_MODEL
from ecoinvent_carbon_balance.lib import get_substances_carbon_content_dict

result_filepath = os.path.join(RESULTS_FOLDER, SYSTEM_MODEL, 'carbon_balance_io.csv')

# Opening the substances carbon content (corrected values)
substances_carbon_content = get_substances_carbon_content_dict()

# Looping on every ecospold file
for filename in tqdm.tqdm(os.listdir(ECOINVENT_IO_FOLDER)):

    # Extracting the process data
    filepath = os.path.join(ECOINVENT_IO_FOLDER, filename)
    process = untangle.parse(filepath)

    try:
        process = process.ecoSpold.childActivityDataset
    except AttributeError:
        process = process.ecoSpold.activityDataset

    # Putting the intermediate exchanges in a list
    if type(process.flowData.intermediateExchange) == list:
        intermediate_exchanges = process.flowData.intermediateExchange
    else:
        intermediate_exchanges = [process.flowData.intermediateExchange, ]

    # Putting the elementary exchanges in a list
    if hasattr(process.flowData, 'elementaryExchange'):
        if type(process.flowData.elementaryExchange) == list:
            elementary_exchanges = process.flowData.elementaryExchange
        else:
            elementary_exchanges = [process.flowData.elementaryExchange, ]
    else:
        elementary_exchanges = []

    # Getting the product flow (Ecoinvent has no coproducts)
    for exchange in intermediate_exchanges:
        try:
            if exchange.outputGroup.cdata == '0':  # 0 is the code for the reference product in ecospold2
                product_flow = exchange
                break
        except AttributeError:
            pass

    for flow in elementary_exchanges:
        if flow.name.cdata == 'Coal, brown, in ground':
            if hasattr(flow, 'property'):
                a=0

"""
Calculation of the importance of each substance in ecoinvent
"""

import os
import csv

import untangle
import tqdm

from ecoinvent_carbon_balance.vars import RESULTS_FOLDER, SUBSTANCES_CARBON_CONTENT_FILEPATH, ECOINVENT_LCI_FOLDER

result_filepath = os.path.join(RESULTS_FOLDER, 'substances_importance.csv')
summary_result_filepath = os.path.join(RESULTS_FOLDER, 'substances_importance_summary.csv')

substances_importance = dict()

# Getting substances variability
with open(SUBSTANCES_CARBON_CONTENT_FILEPATH, 'r') as file:
    reader = csv.reader(file, delimiter=';')

    substances_carbon_content_variability = dict()

    first_row = True
    for row in reader:
        if first_row:
            first_row = False
            continue

        substances_carbon_content_variability[row[0]] = row[3]

# Looping on every ecospold file
for filename in tqdm.tqdm(os.listdir(ECOINVENT_LCI_FOLDER)):

    # Extracting the process data
    filepath = os.path.join(ECOINVENT_LCI_FOLDER, filename)
    process = untangle.parse(filepath)

    try:
        process = process.ecoSpold.childActivityDataset
    except AttributeError:
        process = process.ecoSpold.activityDataset

    # Putting the elementary exchanges in a list
    if hasattr(process.flowData, 'elementaryExchange'):
        if type(process.flowData.elementaryExchange) == list:
            elementary_exchanges = process.flowData.elementaryExchange
        else:
            elementary_exchanges = [process.flowData.elementaryExchange, ]
    else:
        elementary_exchanges = []

    # Summing the substance quantity over each processes
    substances_used_in_this_process = list()
    for flow in elementary_exchanges:
        if flow.name.cdata in substances_importance:
            substances_importance[flow.name.cdata]['amount'] += float(flow['amount'])
            substances_importance[flow.name.cdata]['times_used'] += 1

            if flow.name.cdata not in substances_used_in_this_process:
                substances_importance[flow.name.cdata]['processes_using_it'] += 1
                substances_used_in_this_process.append(flow.name.cdata)

        else:
            substances_importance[flow.name.cdata] = {'amount': float(flow['amount']), 'unit': flow.unitName.cdata,
                                                      'times_used': 1, 'processes_using_it': 1}
            substances_used_in_this_process.append(flow.name.cdata)

    # Adding variability info
    for substance in substances_importance.items():
        try:
            substance[1]['variability'] = substances_carbon_content_variability[substance[0]]
        except KeyError:
            substance[1]['variability'] = 'No carbon'

# Exporting results
with open(result_filepath, 'w', newline='') as file:
    writer = csv.writer(file, delimiter=';')
    writer.writerow(['name', 'unit', 'carbon content variability', 'amount', 'times used', 'processes using it'])

    for substance in substances_importance.items():
        writer.writerow([substance[0], substance[1]['unit'], substance[1]['variability'], substance[1]['amount'],
                         substance[1]['times_used'], substance[1]['processes_using_it']])

# Exporting results summary
with open(summary_result_filepath, 'w', newline='') as file:
    writer = csv.writer(file, delimiter=';')
    writer.writerow(['unit', 'carbon content variability', 'amount', 'times used', 'processes using it'])

    VARIABILITY = ['No carbon', 'fixed', 'variable']
    UNITS = ['kg', 'kBq', 'm2', 'm2*year', 'm3', 'm3*year', 'MJ']

    for variability in VARIABILITY:
        for unit in UNITS:
            writer.writerow([unit,
                             variability,
                             sum([sub[1]['amount'] for sub in substances_importance.items()
                                  if sub[1]['variability'] == variability and sub[1]['unit'] == unit]),
                             sum([sub[1]['times_used'] for sub in substances_importance.items()
                                  if sub[1]['variability'] == variability and sub[1]['unit'] == unit]),
                             sum([sub[1]['processes_using_it'] for sub in substances_importance.items()
                                  if sub[1]['variability'] == variability and sub[1]['unit'] == unit])])

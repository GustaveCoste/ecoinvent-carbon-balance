import math
import re

import numpy as np
from matplotlib import pyplot as plt


def abline(slope, intercept, color='green', ax=None):
    """
    Plot a line from slope and intercept
    https://stackoverflow.com/a/43811762

    Args:
        slope (float):
        intercept (float):
        color (str):

    Returns:

    """
    axes = plt.gca()
    x_vals = np.array(axes.get_xlim())
    y_vals = intercept + slope * x_vals

    if ax is not None:
        ax.plot(x_vals, y_vals, '--', color=color, linewidth=1.5)
    else:
        plt.plot(x_vals, y_vals, '--', color=color, linewidth=1.5)


def rel_diff(x, y):
    """
    Calculates the relative difference between x and y

    Args:
        x (float or int):
        y (float or int):

    Returns:
        float
    """

    try:
        return abs(x - y) / ((abs(x) + abs(y)) / 2)
    except ZeroDivisionError:
        return 0


def log_modulus(nb):
    """
    Log-modulus transformation for positive and negative data representation as described in:
    https://blogs.sas.com/content/iml/2014/07/14/log-transformation-of-pos-neg.html

    Formula:
        f(x) = sign(x) * log(|x| + 1)

    Args:
        nb (float):

    Returns:
        float:
    """

    return math.copysign(math.log10(abs(nb) + 1), nb)


def _improve_ticks_labels(labels):
    for index, value in enumerate(labels):
        # Replacing '10^0' by '1'
        if value == '$\\mathdefault{-10^{0}}$':
            labels[index] = '$\\mathdefault{-1}$'
        if value == '$\\mathdefault{10^{0}}$':
            labels[index] = '$\\mathdefault{1}$'

        # Replacing '10^1' by '10'
        if value == '$\\mathdefault{-10^{1}}$':
            labels[index] = '$\\mathdefault{-10}$'
        if value == '$\\mathdefault{10^{1}}$':
            labels[index] = '$\\mathdefault{10}$'

        # Removing half of labels if too much labels
        if len(labels) > 10:
            re_match = re.search(r'\$\\mathdefault\{-?10\^\{(\d+)\}\}\$', value)
            if re_match:
                if int(re_match.group(1)) % 2 == 0 and int(re_match.group(1)) != 0:
                    labels[index] = ''


def improve_ticks_labels(fig, ax):
    """
    Improves labels of a plot

    Args:
        fig:
        ax:

    Returns:
        list:
    """

    fig.canvas.draw()

    # Asserts at least two ticks are displayed (to avoid cases when only 0 is displayed with no scale).
    if len(ax.get_xticks()) < 2:
        x_ticks = list(ax.get_xticks()) + [-0.1, 0.1]
        ax.set_xticks(x_ticks)

    if len(ax.get_yticks()) < 2 and 'mathdefault' in str(ax.get_yticklabels()[0]):
        ax.set_yticks(list(ax.get_yticks()) + [-0.1, 0.1])

    # Getting labels
    x_labels = [item.get_text() for item in ax.get_xticklabels()]
    y_labels = [item.get_text() for item in ax.get_yticklabels()]

    # Labels transformation
    _improve_ticks_labels(x_labels)
    _improve_ticks_labels(y_labels)

    # Setting labels
    ax.set_xticklabels(x_labels)
    ax.set_yticklabels(y_labels)

    ax.tick_params(axis='x', which='major', pad=20)

    for tick in ax.xaxis.get_major_ticks():
        # Setting x tick labels alignment
        tick.label1.set_verticalalignment('bottom')

        # Setting 0 label font weight and color
        if 'mathdefault{0}' in tick.label1.get_text():
            tick.label1.set_fontweight('bold')
            tick.label1.set_color('#6E0000')

    for tick in ax.yaxis.get_major_ticks():
        # Setting 0 label font weight and color
        if 'mathdefault{0}' in tick.label1.get_text():
            tick.label1.set_fontweight('bold')
            tick.label1.set_color('#6E0000')


def set_axes_same_size(ax=None):
    """
    Sets axes of a plot to the same size.

    Returns:

    """
    if ax is None:

        xmin, xmax = plt.xlim()
        ymin, ymax = plt.ylim()

        plt.xlim(min(xmin, ymin), max(xmax, ymax))
        plt.ylim(min(xmin, ymin), max(xmax, ymax))

    else:
        xmin, xmax = ax.get_xlim()
        ymin, ymax = ax.get_ylim()

        ax.set_xlim(min(xmin, ymin), max(xmax, ymax))
        ax.set_ylim(min(xmin, ymin), max(xmax, ymax))


def magnitude(x):
    return pow(10, int(math.log10(x)))


def get_axes_limits(x, y):
    x_min = min([i for i in x if i != 0] or [0.1])
    x_max = max([i for i in x if i != 0] or [1])
    y_min = min([i for i in y if i != 0] or [0.1])
    y_max = max([i for i in y if i != 0] or [1])

    return magnitude(min(x_min, y_min)) / 10, magnitude(max(x_max, y_max)) * 10

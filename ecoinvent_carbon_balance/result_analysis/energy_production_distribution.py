"""
Script used to explain the separation of energy production processes in two groupes when looking at their distribution
input vs output
"""

import os
import csv

import untangle
import tqdm

import pandas as pd

from ecoinvent_carbon_balance.vars import RESULTS_FOLDER, SYSTEM_MODEL, ECOINVENT_LCI_FOLDER
from ecoinvent_carbon_balance.lib import get_substances_carbon_content_dict

carbon_balance_lci_filepath = os.path.join(RESULTS_FOLDER, SYSTEM_MODEL, 'carbon_balance_lci.csv')
results_filepath = os.path.join(RESULTS_FOLDER, SYSTEM_MODEL, 'energy_processes_cb_substances_contributions.csv')


# Reading substances list
substances = get_substances_carbon_content_dict()

# Importing result data
lci_df = pd.read_csv(carbon_balance_lci_filepath, delimiter=';', encoding="latin1")

# Adjusting data types
lci_df.loc[:, 'isic_code_1': 'isic_code_4'] = lci_df.applymap(str).loc[:, 'isic_code_1': 'isic_code_4']

# Groupe A is the group near the first bisector,
# Group B is the group whith a different slope (overestimated C input or underestimated C output)
group_a_filenames = lci_df.filename[(lci_df['isic_code_1'] == 'D') &
                                    (lci_df['total_output'] >= 0.7 * lci_df['total_input'])]
group_b_filenames = lci_df.filename[(lci_df['isic_code_1'] == 'D') &
                                    (lci_df['total_output'] < 0.7 * lci_df['total_input'])]

inputs_a = dict()
outputs_a = dict()
inputs_b = dict()
outputs_b = dict()


def process_list(filenames, input_dict, output_dict):
    for filename in tqdm.tqdm(filenames):

        # Extracting the process data
        filepath = os.path.join(ECOINVENT_LCI_FOLDER, filename)
        process = untangle.parse(filepath)

        try:
            process = process.ecoSpold.childActivityDataset
        except AttributeError:
            process = process.ecoSpold.activityDataset

        # Putting the elementary exchanges in a list
        if hasattr(process.flowData, 'elementaryExchange'):
            if type(process.flowData.elementaryExchange) == list:
                elementary_exchanges = process.flowData.elementaryExchange
            else:
                elementary_exchanges = [process.flowData.elementaryExchange, ]
        else:
            elementary_exchanges = []

        for flow in elementary_exchanges:

            if flow.name.cdata not in substances:
                continue

            carbon_mass = substances[flow.name.cdata] * float(flow['amount'])

            if hasattr(flow, 'inputGroup'):
                if flow.name.cdata not in input_dict:
                    input_dict[flow.name.cdata] = (1, carbon_mass)
                else:
                    input_dict[flow.name.cdata] = (input_dict[flow.name.cdata][0] + 1,
                                                   input_dict[flow.name.cdata][1] + carbon_mass)

            if hasattr(flow, 'outputGroup'):
                if flow.name.cdata not in output_dict:
                    output_dict[flow.name.cdata] = (1, carbon_mass)
                else:
                    output_dict[flow.name.cdata] = (output_dict[flow.name.cdata][0] + 1,
                                                    output_dict[flow.name.cdata][1] + carbon_mass)


process_list(group_a_filenames, inputs_a, outputs_a)
process_list(group_b_filenames, inputs_b, outputs_b)

# Writting results
with open(results_filepath, 'w', newline='') as file:
    writer = csv.writer(file, delimiter=';')

    writer.writerow(['substance', 'type', 'nb A', 'total A', 'mean A', 'nb B', 'total B', 'mean B'])

    for substance in substances.keys():
        if (substance in inputs_a) or (substance in inputs_b):
            try:
                nb_a = inputs_a[substance][0]
                tot_a = inputs_a[substance][1]
                mean_a = tot_a / nb_a
            except KeyError:
                nb_a = 0
                tot_a = 0
                mean_a = 0

            try:
                nb_b = inputs_b[substance][0]
                tot_b = inputs_b[substance][1]
                mean_b = tot_b / nb_b
            except KeyError:
                nb_b = 0
                tot_b = 0
                mean_b = 0

            writer.writerow([substance, 'input', nb_a, tot_a, mean_a, nb_b, tot_b, mean_b])

        elif (substance in outputs_a) or (substance in outputs_b):
            try:
                nb_a = outputs_a[substance][0]
                tot_a = outputs_a[substance][1]
                mean_a = tot_a / nb_a
            except KeyError:
                nb_a = 0
                tot_a = 0
                mean_a = 0

            try:
                nb_b = outputs_b[substance][0]
                tot_b = outputs_b[substance][1]
                mean_b = tot_b / nb_b
            except KeyError:
                nb_b = 0
                tot_b = 0
                mean_b = 0

            writer.writerow([substance, 'output', nb_a, tot_a, mean_a, nb_b, tot_b, mean_b])

        else:
            continue

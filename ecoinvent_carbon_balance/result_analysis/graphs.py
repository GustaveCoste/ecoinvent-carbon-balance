"""
Graphs for carbon balance analysis
"""

import os
import textwrap

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns
import pandas as pd
import numpy as np
from scipy.stats import ttest_ind

from ecoinvent_carbon_balance.vars import GRAPHS_FOLDER, RESULTS_FOLDER, SYSTEM_MODEL
from ecoinvent_carbon_balance.result_analysis.lib import abline, improve_ticks_labels, set_axes_same_size

sns.set(color_codes=True)

# Importing result data
carbon_balance_lci_filepath = os.path.join(RESULTS_FOLDER, SYSTEM_MODEL, 'carbon_balance_lci.csv')
carbon_balance_io_filepath = os.path.join(RESULTS_FOLDER, SYSTEM_MODEL, 'carbon_balance_io.csv')
coal_based_processes_filepath = os.path.join(RESULTS_FOLDER, SYSTEM_MODEL, 'coal_based_processes.csv')

lci_df = pd.read_csv(carbon_balance_lci_filepath, delimiter=';', encoding="latin1")
io_df = pd.read_csv(carbon_balance_io_filepath, delimiter=';', encoding="latin1")
coal_based_processes = list(pd.read_csv(coal_based_processes_filepath,
                                        delimiter=';', encoding="latin1", header=None)[0])

# Adjusting data types
lci_df.loc[:, 'isic_code_1': 'isic_code_4'] = lci_df.applymap(str).loc[:, 'isic_code_1': 'isic_code_4']
io_df.loc[:, 'isic_code_1': 'isic_code_4'] = io_df.applymap(str).loc[:, 'isic_code_1': 'isic_code_4']

COLORS = ['#00B7BE', '#FE5745', '#7F7FCF', '#F39D01', '#8FB603', '#FBCEB6']

EXPORT_IMAGE = False
BY_ISIC_CODE = False
IDENTIFY_POINTS = False
LOG = False
LOG_HIST = False
ABS_TECH_FLOWS = False
SHOW_BOXPLOT_OUTLIERS = False
LCI_VS_IO = False
IO_VS_IO = False
INPUT_VS_OUTPUT_LCI = False
INPUT_VS_OUTPUT_IO = False
CARBON_BALANCE_DISTRIBUTION_LCI = False
CARBON_BALANCE_DISTRIBUTION_IO = False
CARBON_BALANCE_BOXPLOTS_LCI = False
CARBON_BALANCE_BOXPLOTS_IO = False
PRODUCT_VS_OTHER_LCI = False
PRODUCT_VS_OTHER_IO = False
#######################


EXPORT_IMAGE = True
BY_ISIC_CODE = True
IDENTIFY_POINTS = True
LOG = True
LOG_HIST = True
# SHOW_BOXPLOT_OUTLIERS = True

HIST_BINS = 20
ISIC_LEVEL = 1

# LCI_VS_IO = True
INPUT_VS_OUTPUT_LCI = True
# INPUT_VS_OUTPUT_IO = True
# CARBON_BALANCE_DISTRIBUTION_LCI = True
# CARBON_BALANCE_DISTRIBUTION_IO = True
# CARBON_BALANCE_BOXPLOTS_LCI = True
# CARBON_BALANCE_BOXPLOTS_IO = True
# PRODUCT_VS_OTHER_LCI = True
# PRODUCT_VS_OTHER_IO = True

#########################
# Semi-logaritmic scale
if LOG:
    SCALE = 'symlog'
else:
    SCALE = 'linear'

# Identifing points
io_df['identify'] = [0] * io_df.shape[0]
lci_df['identify'] = [0] * lci_df.shape[0]
if IDENTIFY_POINTS:
    # Conditions on 1 dataset only
    id_list_1 = coal_based_processes
    id_list_3 = []
    id_list_2 = []

    # Conditions on both datasets
    # id_list_4_lci = lci_df.filename[lci_df['carbon_balance'] > 1]
    # id_list_4_io = io_df.filename[io_df['carbon_balance'] < -1]
    id_list_4_lci = []
    id_list_4_io = []

    id_list_5_lci = []
    id_list_5_io = []

    # Evaluating conditions
    id_list_4 = [x for x in id_list_4_lci if x in list(id_list_4_io)]
    id_list_5 = [x for x in id_list_5_lci if x in list(id_list_5_io)]

    io_df.loc[io_df.filename.isin(id_list_1), 'identify'] = 1
    io_df.loc[io_df.filename.isin(id_list_3), 'identify'] = 3
    io_df.loc[io_df.filename.isin(id_list_4), 'identify'] = 4
    io_df.loc[io_df.filename.isin(id_list_5), 'identify'] = 5
    io_df.loc[io_df.filename.isin(id_list_2), 'identify'] = 5

    lci_df.loc[lci_df.filename.isin(id_list_1), 'identify'] = 1
    lci_df.loc[lci_df.filename.isin(id_list_3), 'identify'] = 3
    lci_df.loc[lci_df.filename.isin(id_list_4), 'identify'] = 4
    lci_df.loc[lci_df.filename.isin(id_list_5), 'identify'] = 5
    lci_df.loc[lci_df.filename.isin(id_list_2), 'identify'] = 5

    # Sorting data by identify order
    lci_df = lci_df.sort_values('identify')
    io_df = io_df.sort_values('identify')

# Ploting lci carbon balance against io carbon balance
if LCI_VS_IO:
    df1 = lci_df[['filename', 'carbon_balance', 'identify']]
    df2 = io_df[['filename', 'carbon_balance', 'identify']]

    df = pd.merge(df1, df2, left_on='filename', right_on='filename', how='inner',
                  suffixes=('_lci', '_io'))

    # Plotting
    fig, ax = plt.subplots()
    ax.scatter(x=df['carbon_balance_lci'], y=df['carbon_balance_io'], marker='x',
               c=[COLORS[x] for x in df['identify_io']])
    ax.set_xscale(SCALE)
    ax.set_yscale(SCALE)

    # Setting axes to the same size
    set_axes_same_size()

    # Changing tick labels
    improve_ticks_labels(fig, ax)

    # Setting axes labels
    plt.xlabel('lci')
    plt.ylabel('io')

    plt.title('LCI Vs IO')

    # Adding first bisector
    abline(1, 0, 'red')

    if EXPORT_IMAGE:
        filename = os.path.join(GRAPHS_FOLDER, 'LCI Vs IO')
        plt.savefig(filename)
        plt.clf()
    else:
        plt.show()

# Plotting inputs vs outputs for LCI
if INPUT_VS_OUTPUT_LCI:
    if BY_ISIC_CODE:
        for isic in lci_df.isic_code_1.unique():

            df = lci_df[lci_df.isic_code_1 == isic]

            # Computing correlation_index and p-value
            correlation_index = np.corrcoef(x=df['total_input'], y=df['total_output'])[0][1]
            t, p_value = ttest_ind(df['total_input'], df['total_output'])

            # Plotting
            fig, ax = plt.subplots()
            ax.scatter(x=df['total_input'], y=df['total_output'], marker='x',
                       c=[COLORS[x] for x in df['identify']])
            ax.set_xscale(SCALE)
            ax.set_yscale(SCALE)

            # Positionning the label at the right of the graph
            label_x = max([df['total_input'].max(), df['total_output'].max()])
            label_y = min([df['total_input'].min(), df['total_output'].min()])

            ax.text(label_x, label_y, "R: {:0.3f}\np-value: {:0.3f}".format(correlation_index, p_value),
                    fontweight='bold',
                    horizontalalignment='right')

            # Setting axes to the same size
            set_axes_same_size()

            # Changing tick labels
            improve_ticks_labels(fig, ax)

            desc = df.isic_desc_1.unique()[0]

            if str(desc) == 'nan':
                desc = 'No category'

            plt.title('Input Vs Output LCI\n' + str(desc))

            # Adding first bisector
            abline(1, 0, 'red')

            # Setting axes labels
            plt.xlabel('Input')
            plt.ylabel('Output')

            results_folder = os.path.join(GRAPHS_FOLDER, 'input vs output', 'LCI', 'Level {}'.format(str(ISIC_LEVEL)))
            filename = os.path.join(results_folder, 'input_vs_output_lci_{}'.format(desc))
            plt.savefig(filename)
            plt.clf()
    else:

        # Computing correlation_index and p-value
        correlation_index = np.corrcoef(x=lci_df['total_input'], y=lci_df['total_output'])[0][1]
        t, p_value = ttest_ind(lci_df['total_input'], lci_df['total_output'])

        fig, ax = plt.subplots()
        ax.scatter(x=lci_df['total_input'], y=lci_df['total_output'], marker='x',
                   c=[COLORS[x] for x in lci_df['identify']])

        # Positionning the label at the right of the graph
        label_x = max([lci_df['total_input'].max(), lci_df['total_output'].max()])
        label_y = min([lci_df['total_input'].min(), lci_df['total_output'].min()])

        ax.text(label_x, label_y, "R: {:0.3f}\np-value: {:0.3f}".format(correlation_index, p_value), fontweight='bold',
                horizontalalignment='right')

        ax.set_xscale(SCALE)
        ax.set_yscale(SCALE)

        # Setting axes to the same size
        set_axes_same_size()

        # Changing tick labels
        improve_ticks_labels(fig, ax)

        plt.title('Input Vs Output LCI')

        # Setting axes labels
        plt.xlabel('Input')
        plt.ylabel('Output')

        # Adding first bisector
        abline(1, 0, 'red')

        if EXPORT_IMAGE:
            filename = os.path.join(GRAPHS_FOLDER, 'Input Vs Output LCI')
            plt.savefig(filename)
            plt.clf()
        else:
            plt.show()

# Plotting inputs vs outputs for IO
if INPUT_VS_OUTPUT_IO:

    if BY_ISIC_CODE:
        for isic in io_df.isic_code_1.unique():

            df = io_df[io_df.isic_code_1 == isic]

            fig, ax = plt.subplots()
            ax.scatter(x=df['total_input'], y=df['total_output'], marker='x',
                       c=[COLORS[x] for x in df['identify']])
            ax.set_xscale(SCALE)
            ax.set_yscale(SCALE)

            # Setting axes to the same size
            set_axes_same_size()

            # Changing tick labels
            improve_ticks_labels(fig, ax)

            desc = df.isic_desc_1.unique()[0]

            if str(desc) == 'nan':
                desc = 'No category'

            plt.title('Input Vs Output IO\n' + str(desc))

            # Adding first bisector
            abline(1, 0, 'red')

            # Setting axes labels
            plt.xlabel('Input')
            plt.ylabel('Output')

            results_folder = os.path.join(GRAPHS_FOLDER, 'input vs output', 'IO', 'Level {}'.format(str(ISIC_LEVEL)))
            filename = os.path.join(results_folder, 'input_vs_output_io_{}'.format(desc))
            plt.savefig(filename)
            plt.clf()
    else:
        fig, ax = plt.subplots()
        ax.scatter(x=io_df['total_input'], y=io_df['total_output'], marker='x',
                   c=[COLORS[x] for x in io_df['identify']])
        ax.set_xscale(SCALE)
        ax.set_yscale(SCALE)

        # Setting axes to the same size
        set_axes_same_size()

        # Changing tick labels
        improve_ticks_labels(fig, ax)

        plt.title('Input Vs Output IO')

        # Setting axes labels
        plt.xlabel('Input')
        plt.ylabel('Output')

        # Adding first bisector
        abline(1, 0, 'red')

    if EXPORT_IMAGE:
        filename = os.path.join(GRAPHS_FOLDER, 'Input Vs Output IO')
        plt.savefig(filename)
        plt.clf()
    else:
        plt.show()

# Plotting carbon balance distribution for LCI
if CARBON_BALANCE_DISTRIBUTION_LCI:

    if BY_ISIC_CODE:
        for isic in lci_df.isic_code_1.unique():

            df = lci_df[lci_df.isic_code_1 == isic]

            fig, ax = plt.subplots()
            ax.hist(df['carbon_balance'], bins=HIST_BINS)
            ax.set_xscale(SCALE)
            ax.set_yscale(SCALE)

            plt.hist(df['carbon_balance'], bins=HIST_BINS)
            if LOG_HIST:
                ax.set_yscale('log', nonposy='clip')
            ax.set_xscale(SCALE)

            # Changing tick labels
            improve_ticks_labels(fig, ax)

            desc = df.isic_desc_1.unique()[0]

            if str(desc) == 'nan':
                desc = 'No category'

            plt.title('Carbon balance distribution LCI\n' + str(desc))

            filename = os.path.join(GRAPHS_FOLDER, 'carbon_balance_distribution_lci_{}'.format(desc))
            plt.savefig(filename)
            plt.clf()
    else:

        fig, ax = plt.subplots()
        ax.hist(lci_df['carbon_balance'], bins=HIST_BINS, histtype='step')
        ax.set_xscale(SCALE)
        ax.set_yscale(SCALE)

        if LOG_HIST:
            ax.set_yscale('log', nonposy='clip')
        ax.set_xscale(SCALE)

        # Changing tick labels
        improve_ticks_labels(fig, ax)

        plt.title('Carbon balance distribution LCI')

        if EXPORT_IMAGE:
            filename = os.path.join(GRAPHS_FOLDER, 'Carbon balance distribution LCI')
            plt.savefig(filename)
            plt.clf()
        else:
            plt.show()

# Plotting carbon balance distribution for IO
if CARBON_BALANCE_DISTRIBUTION_IO:

    if BY_ISIC_CODE:
        for isic in io_df.isic_code_2.unique():

            df = io_df[io_df.isic_code_2 == isic]

            fig, ax = plt.subplots()
            ax.hist(df['carbon_balance'], bins=HIST_BINS)
            ax.set_xscale(SCALE)
            ax.set_yscale(SCALE)

            plt.hist(df['carbon_balance'], bins=HIST_BINS)
            if LOG_HIST:
                ax.set_yscale('log', nonposy='clip')
            ax.set_xscale(SCALE)

            # Changing tick labels
            improve_ticks_labels(fig, ax)

            desc = df.isic_desc_2.unique()[0]

            if str(desc) == 'nan':
                desc = 'No category'

            plt.title('Carbon balance distribution IO\n' + str(desc))

            filename = os.path.join(GRAPHS_FOLDER, 'carbon_balance_distribution_io_{}'.format(desc))
            plt.savefig(filename)
            plt.clf()
    else:
        fig, ax = plt.subplots()
        ax.hist(io_df['carbon_balance'], bins=HIST_BINS, histtype='step')
        ax.set_xscale(SCALE)
        ax.set_yscale(SCALE)

        if LOG_HIST:
            ax.set_yscale('log', nonposy='clip')
        ax.set_xscale(SCALE)

        # Changing tick labels
        improve_ticks_labels(fig, ax)

        plt.title('Carbon balance distribution IO')

        if EXPORT_IMAGE:
            filename = os.path.join(GRAPHS_FOLDER, 'Carbon balance distribution IO')
            plt.savefig(filename)
            plt.clf()
        else:
            plt.show()

# Plotting carbon balance distribution as boxplots for LCI
if CARBON_BALANCE_BOXPLOTS_LCI:

    if BY_ISIC_CODE:
        for isic in lci_df.isic_code_1.unique():
            df = lci_df[lci_df.isic_code_1 == isic]

            isic_sub = df.isic_code_2.unique()

            data = [df[df.isic_code_2 == x]['carbon_balance'] for x in isic_sub]
            labels = [df[df.isic_code_2 == x]['isic_desc_2'].unique()[0] for x in isic_sub]
            labels = ['\n'.join(textwrap.wrap(l, 25)) for l in labels]

            desc = df.isic_desc_1.unique()[0]

            height = 3 + len(data) * 0.5

            fig, ax = plt.subplots()
            fig.set_size_inches(8, height)
            ax.boxplot(data, labels=labels, vert=False, showfliers=SHOW_BOXPLOT_OUTLIERS)
            ax.set_xscale(SCALE)

            # Changing tick labels
            improve_ticks_labels(fig, ax)

            plt.title('Carbon balance distribution LCI\n' + str(desc))
            fig.tight_layout()

            results_folder = os.path.join(GRAPHS_FOLDER, 'carbon balance distribution', 'LCI',
                                          'Level {}'.format(str(ISIC_LEVEL)))
            filename = os.path.join(results_folder, desc)

            fig.savefig(filename, dpi=80)
            plt.clf()

    else:

        # Computing relative difference
        lci_df['relative_difference'] = abs(lci_df['total_output'] - lci_df['total_input']) / (
                (abs(lci_df['total_output']) + abs(lci_df['total_input'])) / 2)

        isic_sub = lci_df.isic_code_1.unique()

        data = [lci_df[lci_df.isic_code_1 == x][lci_df['relative_difference'].notnull()]['relative_difference']
                for x in isic_sub]
        labels = [lci_df[lci_df.isic_code_1 == x][lci_df['relative_difference'].notnull()]['isic_desc_1'].unique()[0]
                  for x in isic_sub]
        labels = ['\n'.join(textwrap.wrap(l, 25)) for l in labels]

        fig = plt.figure()
        gs = gridspec.GridSpec(2, 1, height_ratios=[1, 8])

        ax1 = fig.add_subplot(gs[0])
        ax2 = fig.add_subplot(gs[1], sharex=ax1)

        ax1.boxplot(lci_df['relative_difference'][lci_df['relative_difference'].notnull()], labels=['All categories'],
                    vert=False, showfliers=SHOW_BOXPLOT_OUTLIERS, widths=[0.35])
        ax1.set_title('Distribution of carbon input and output relative difference')

        ax2.boxplot(data, labels=labels, vert=False, showfliers=SHOW_BOXPLOT_OUTLIERS)

        # Changing tick labels
        improve_ticks_labels(fig, ax2)

        height = 4 + len(data) * 0.4
        fig.set_size_inches(8, height)
        plt.tight_layout()

        if EXPORT_IMAGE:
            filename = os.path.join(GRAPHS_FOLDER, 'Carbon balance boxplots LCI')
            plt.savefig(filename)
            plt.clf()
        else:
            plt.show()

# Plotting carbon balance distribution as boxplots for IO
if CARBON_BALANCE_BOXPLOTS_IO:

    if BY_ISIC_CODE:
        for isic in io_df.isic_code_1.unique():
            df = io_df[io_df.isic_code_1 == isic]

            isic_sub = df.isic_code_2.unique()

            data = [df[df.isic_code_2 == x]['carbon_balance'] for x in isic_sub]
            labels = [df[df.isic_code_2 == x]['isic_desc_2'].unique()[0] for x in isic_sub]
            labels = ['\n'.join(textwrap.wrap(l, 25)) for l in labels]

            desc = df.isic_desc_1.unique()[0]

            height = 3 + len(data) * 0.5

            fig, ax = plt.subplots()
            fig.set_size_inches(8, height)
            ax.boxplot(data, labels=labels, vert=False, showfliers=SHOW_BOXPLOT_OUTLIERS)
            ax.set_xscale(SCALE)

            # Changing tick labels
            improve_ticks_labels(fig, ax)

            plt.title('Carbon balance distribution IO\n' + str(desc))
            fig.tight_layout()

            results_folder = os.path.join(GRAPHS_FOLDER, 'input vs output', 'LCI', 'Level {}'.format(str(ISIC_LEVEL)))
            filename = os.path.join(results_folder, desc)

            fig.savefig(filename, dpi=80)
            plt.clf()

    else:
        fig, ax = plt.subplots()
        ax.boxplot(io_df['carbon_balance'], vert=False)
        ax.set_xscale(SCALE)

        # Changing tick labels
        improve_ticks_labels(fig, ax)

        plt.title('Carbon balance distribution IO')

        if EXPORT_IMAGE:
            filename = os.path.join(GRAPHS_FOLDER, 'Carbon balance boxplots IO')
            plt.savefig(filename)
            plt.clf()
        else:
            plt.show()

# Plotting product vs other flows for LCI
if PRODUCT_VS_OTHER_LCI:

    if BY_ISIC_CODE:
        for isic in lci_df.isic_code_1.unique():

            df = lci_df[lci_df.isic_code_1 == isic]

            fig, ax = plt.subplots()
            ax.scatter(x=df['total_input'] - df['biosphere_output'], y=df['technosphere_output'], marker='x',
                       c=[COLORS[x] for x in df['identify']])
            ax.set_xscale(SCALE)
            ax.set_yscale(SCALE)

            # Setting axes to the same size
            set_axes_same_size()

            # Changing tick labels
            improve_ticks_labels(fig, ax)

            desc = df.isic_desc_1.unique()[0]

            if str(desc) == 'nan':
                desc = 'No category'

            plt.title('Product Vs other flows LCI\n' + str(desc))

            # Adding first bisector
            abline(1, 0, 'red')

            # Setting axes labels
            plt.xlabel('Other flows')
            plt.ylabel('Product')

            results_folder = os.path.join(GRAPHS_FOLDER, 'product vs other', 'LCI', 'Level {}'.format(str(ISIC_LEVEL)))
            filename = os.path.join(results_folder, 'product_vs_other_lci_{}'.format(desc))
            plt.savefig(filename)
            plt.clf()
    else:

        fig, ax = plt.subplots()
        ax.scatter(x=lci_df['total_input'] - lci_df['biosphere_output'], y=lci_df['technosphere_output'],
                   marker='x', c=[COLORS[x] for x in lci_df['identify']])
        ax.set_xscale(SCALE)
        ax.set_yscale(SCALE)

        # Setting axes to the same size
        set_axes_same_size()

        # Changing tick labels
        improve_ticks_labels(fig, ax)

        plt.title('Product Vs other flows LCI')

        # Setting axes labels
        plt.xlabel('Other flows')
        plt.ylabel('Product')

        # Adding first bisector
        abline(1, 0, 'red')

        if EXPORT_IMAGE:
            filename = os.path.join(GRAPHS_FOLDER, 'Product Vs other flows LCI')
            plt.savefig(filename)
            plt.clf()
        else:
            plt.show()

# Plotting product vs other flows for IO
if PRODUCT_VS_OTHER_IO:

    if BY_ISIC_CODE:
        for isic in io_df.isic_code_1.unique():

            df = io_df[io_df.isic_code_1 == isic]

            fig, ax = plt.subplots()
            ax.scatter(x=df['total_input'] - df['biosphere_output'], y=df['technosphere_output'], marker='x',
                       c=[COLORS[x] for x in df['identify']])
            ax.set_xscale(SCALE)
            ax.set_yscale(SCALE)

            # Setting axes to the same size
            set_axes_same_size()

            # Changing tick labels
            improve_ticks_labels(fig, ax)

            desc = df.isic_desc_1.unique()[0]

            if str(desc) == 'nan':
                desc = 'No category'

            plt.title('Product Vs other flows IO\n' + str(desc))

            # Adding first bisector
            abline(1, 0, 'red')

            # Setting axes labels
            plt.xlabel('Other flows')
            plt.ylabel('Product')

            results_folder = os.path.join(GRAPHS_FOLDER, 'product vs other', 'IO', 'Level {}'.format(str(ISIC_LEVEL)))
            filename = os.path.join(results_folder, 'product_vs_other_io_{}'.format(desc))
            plt.savefig(filename)
            plt.clf()
    else:
        fig, ax = plt.subplots()
        ax.scatter(x=io_df['total_input'] - io_df['biosphere_output'], y=io_df['technosphere_output'], marker='x',
                   c=[COLORS[x] for x in io_df['identify']])
        ax.set_xscale(SCALE)
        ax.set_yscale(SCALE)

        # Setting axes to the same size
        set_axes_same_size()

        # Changing tick labels
        improve_ticks_labels(fig, ax)

        plt.title('Product Vs other flows IO')

        # Setting axes labels
        plt.xlabel('Other flows')
        plt.ylabel('Product')

        # Adding first bisector
        abline(1, 0, 'red')

        if EXPORT_IMAGE:
            filename = os.path.join(GRAPHS_FOLDER, 'Product Vs other flows IO')
            plt.savefig(filename)
            plt.clf()
        else:
            plt.show()

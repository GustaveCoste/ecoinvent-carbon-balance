"""
Script computing a list of errors of substances carbon content in Ecoinvent processes
"""

import os
import csv

import untangle
import tqdm

from ecoinvent_carbon_balance.vars import ECOINVENT_IO_FOLDER, RESULTS_FOLDER, SYSTEM_MODEL
from ecoinvent_carbon_balance.lib import get_flow_carbon_content, get_substances_carbon_content_dict

result_filepath = os.path.join(RESULTS_FOLDER, SYSTEM_MODEL, 'substances_carbon_content_errors.csv')

# Opening the substances carbon content (corrected values)
substances_carbon_content = get_substances_carbon_content_dict()

# Looping on every ecospold file
with open(result_filepath, 'w', newline='') as result:
    writer = csv.writer(result, delimiter=';')

    writer.writerow(['filename', 'substance', 'unit', 'found value', 'reference value', 'relative difference'])

    for filename in tqdm.tqdm(os.listdir(ECOINVENT_IO_FOLDER)):

        # Extracting the process data
        filepath = os.path.join(ECOINVENT_IO_FOLDER, filename)
        process = untangle.parse(filepath)

        try:
            process = process.ecoSpold.childActivityDataset
        except AttributeError:
            process = process.ecoSpold.activityDataset

        # Putting the elementary exchanges in a list
        if hasattr(process.flowData, 'elementaryExchange'):
            if type(process.flowData.elementaryExchange) == list:
                elementary_exchanges = process.flowData.elementaryExchange
            else:
                elementary_exchanges = [process.flowData.elementaryExchange, ]
        else:
            elementary_exchanges = []

        # Looping on elementary exchanges to spot carbon content errors
        for flow in elementary_exchanges:

            try:
                reference_carbon_content = substances_carbon_content[flow.name.cdata]
            except KeyError:
                continue

            dataset_carbon_content = get_flow_carbon_content(flow)

            try:
                relative_difference = abs(reference_carbon_content - dataset_carbon_content) / \
                                  ((reference_carbon_content + dataset_carbon_content) / 2)
            except ZeroDivisionError:
                continue

            if (relative_difference >= 0.01) and (dataset_carbon_content != 0):
                writer.writerow([filename, flow.name.cdata, flow.unitName.cdata, dataset_carbon_content,
                                 reference_carbon_content, relative_difference])

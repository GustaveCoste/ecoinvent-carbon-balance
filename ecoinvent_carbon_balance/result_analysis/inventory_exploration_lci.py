"""
Script for data exploration (with debbuger)
"""

import os

import untangle
import tqdm

from ecoinvent_carbon_balance.vars import ECOINVENT_LCI_FOLDER, RESULTS_FOLDER, SYSTEM_MODEL
from ecoinvent_carbon_balance.lib import get_substances_carbon_content_dict

result_filepath = os.path.join(RESULTS_FOLDER, SYSTEM_MODEL, 'carbon_balance_lci.csv')

# Opening the substances carbon content (corrected values)
substances_carbon_content = get_substances_carbon_content_dict()

# Looping on every ecospold file
for filename in tqdm.tqdm(os.listdir(ECOINVENT_LCI_FOLDER)):

    # Extracting the process data
    filepath = os.path.join(ECOINVENT_LCI_FOLDER, filename)
    process = untangle.parse(filepath)

    try:
        process = process.ecoSpold.childActivityDataset
    except AttributeError:
        process = process.ecoSpold.activityDataset

    # Putting the elementary exchanges in a list
    if hasattr(process.flowData, 'elementaryExchange'):
        if type(process.flowData.elementaryExchange) == list:
            elementary_exchanges = process.flowData.elementaryExchange
        else:
            elementary_exchanges = [process.flowData.elementaryExchange, ]
    else:
        elementary_exchanges = []

    # Getting the product flow (LCI files has only one intermediate exchange: the product)
    assert process.flowData.intermediateExchange.outputGroup.cdata == '0' \
           and process.flowData.intermediateExchange['amount'] != '0'
    product_flow = process.flowData.intermediateExchange

    for flow in elementary_exchanges:
        if flow.name.cdata == 'Oils, unspecified':
            a=0

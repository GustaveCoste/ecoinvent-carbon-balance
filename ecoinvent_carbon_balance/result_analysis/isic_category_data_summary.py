"""
Script to explore the carbon balance data per isic category
"""
import os
import csv
import math

import pandas as pd

from ecoinvent_carbon_balance.vars import RESULTS_FOLDER, SYSTEM_MODEL


result_filepath = os.path.join(RESULTS_FOLDER, SYSTEM_MODEL, 'isic_data_summary.csv')

carbon_balance_lci_filepath = os.path.join(RESULTS_FOLDER, SYSTEM_MODEL, 'carbon_balance_lci.csv')

lci_df = pd.read_csv(carbon_balance_lci_filepath, delimiter=';', encoding="latin1")

data_summaries = list()


def magnitude(x):
    if x == 0:
        return 0

    return int(math.floor(math.log10(x)))


magnitudes_global = set([magnitude(x) for x in lci_df['total_input']] + [magnitude(x) for x in lci_df['total_output']])
magnitudes_global = sorted(magnitudes_global)

for category in lci_df.isic_desc_1.unique():
    # Filtering data
    df = lci_df[lci_df.isic_desc_1 == category]

    data_summary = {'category': category,
                    'nb_data': len(df.filename),
                    'nb_zeros': sum((df['total_input'] == 0) | (df['total_output'] == 0))}
    magnitudes = [magnitude(x) for x in df['total_input']] + [magnitude(x) for x in df['total_output']]
    data_summary['nb_OM'] = len(set(magnitudes))

    for mag in magnitudes_global:
        subset = df[((df['total_input'] >= pow(10, mag)) & (df['total_input'] < pow(10, mag + 1))) |
                    ((df['total_output'] >= pow(10, mag)) & (df['total_output'] < pow(10, mag + 1)))]
        data_summary[str(mag)] = len(subset['filename'])

    data_summaries.append(data_summary)

with open(result_filepath, 'w', newline='') as file:
    writer = csv.DictWriter(file, delimiter=';', fieldnames=data_summary.keys())

    writer.writeheader()
    writer.writerows(data_summaries)

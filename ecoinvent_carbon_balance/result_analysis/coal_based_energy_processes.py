"""
Script for getting a list of energy processes mostly based on coal carbon input
(more thant 50% of carbon input comes from coal)
"""

import os
import csv

import untangle
import tqdm

import pandas as pd

from ecoinvent_carbon_balance.vars import RESULTS_FOLDER, SYSTEM_MODEL, ECOINVENT_LCI_FOLDER
from ecoinvent_carbon_balance.lib import get_substances_carbon_content_dict

carbon_balance_lci_filepath = os.path.join(RESULTS_FOLDER, SYSTEM_MODEL, 'carbon_balance_lci.csv')
results_filepath = os.path.join(RESULTS_FOLDER, SYSTEM_MODEL, 'coal_based_processes.csv')

# Reading substances list
substances = get_substances_carbon_content_dict()

# Importing result data
all_processes = pd.read_csv(carbon_balance_lci_filepath, delimiter=';', encoding="latin1")

# Adjusting data types
energy_processes_filepaths = all_processes[all_processes['isic_code_1'] == 'D']['filename']

# Creating a list of coal based processes
coal_based_processes = list()

for filename in tqdm.tqdm(energy_processes_filepaths):

    # Extracting the process data
    filepath = os.path.join(ECOINVENT_LCI_FOLDER, filename)
    process = untangle.parse(filepath)
    try:
        process = process.ecoSpold.childActivityDataset
    except AttributeError:
        process = process.ecoSpold.activityDataset

    # Putting the elementary exchanges in a list
    if hasattr(process.flowData, 'elementaryExchange'):
        if type(process.flowData.elementaryExchange) == list:
            elementary_exchanges = process.flowData.elementaryExchange
        else:
            elementary_exchanges = [process.flowData.elementaryExchange, ]
    else:
        elementary_exchanges = []

    # Getting the carbon inputs
    carbon_inputs = [flow for flow in elementary_exchanges
                     if flow.name.cdata in substances and
                     hasattr(flow, 'inputGroup')]

    # Computing the total carbon input mass
    total_carbon_mass = sum([float(flow['amount']) * substances[flow.name.cdata] for flow in carbon_inputs])

    # Computing the coal carbon mass
    coal_carbon_mass = sum([float(flow['amount']) * substances[flow.name.cdata] for flow in carbon_inputs
                            if flow.name.cdata in ('Coal, hard, unspecified, in ground', 'Coal, brown, in ground')])

    # If the coal represents more than half of the carbon inputs, adds the process name to the list
    try:
        if coal_carbon_mass / total_carbon_mass >= 0.5:
            coal_based_processes.append(filename)
    except ZeroDivisionError:
        pass

# Exports the list
with open(results_filepath, 'w', newline='') as file:

    for process in coal_based_processes:
        file.write(process + '\n')

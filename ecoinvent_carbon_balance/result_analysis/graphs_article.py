"""
Graphs used in the article
"""

import os
import textwrap

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns
import pandas as pd
import numpy as np
from scipy.stats import ttest_ind

from ecoinvent_carbon_balance.vars import BASE_FOLDER, RESULTS_FOLDER, SYSTEM_MODEL
from ecoinvent_carbon_balance.result_analysis.lib import abline, get_axes_limits, rel_diff

EXPORT_IMAGE = False
SHOW_BOXPLOT_OUTLIERS = False
RELATIVE_DIFFERENCE_BOXPLOT_X_IN_PROPORTION = False

INPUT_VS_OUTPUT = False
INPUT_VS_OUTPUT_PER_CATEGORY = False
INPUT_VS_OUTPUT_ENERGY = False
ENERGY_PROCESSES_RELATIVE_DIFFERENCE_HISTOGRAM = False
RELATIVE_DIFFERENCE_BOXPLOTS = False
PRODUCT_CARBON_CONTENT_RELATIVE_ERROR = False
PRODUCT_CARBON_CONTENT_RELATIVE_ERROR_PER_CATEGORY = False
PRODUCT_CARBON_CONTENT_RATIO = False
PRODUCT_CARBON_CONTENT_RATIO_PER_CATEGORY = False
RELATIVE_DIFFERENCE_VS_RATIO = False
RELATIVE_DIFFERENCE_VS_RELATIVE_ERROR = False
RELATIVE_DIFFERENCE_CURVE = False
RELATIVE_DIFFERENCE_DISTRIBUTION = False
RELATIVE_DIFFERENCE_DISTRIBUTION_PER_CATEGORY = False
PRODUCT_CARBON_CONTENT_ESTIMATED_VS_THEORICAL = False
RATIO_AND_RELATIVE_DIFFERENCE_DISTRIBUTION = False

#####################################

EXPORT_IMAGE = True
# SHOW_BOXPLOT_OUTLIERS = True
# RELATIVE_DIFFERENCE_BOXPLOT_X_IN_PROPORTION = True

# DATA_TYPE = 'U'
DATA_TYPE = 'S'

# PRODUCT_TYPE = 'Normal'
# PRODUCT_TYPE = 'Waste'
PRODUCT_TYPE = 'Total'

# INPUT_VS_OUTPUT = True
# INPUT_VS_OUTPUT_PER_CATEGORY = True
# INPUT_VS_OUTPUT_ENERGY = True
# ENERGY_PROCESSES_RELATIVE_DIFFERENCE_HISTOGRAM = True
# RELATIVE_DIFFERENCE_BOXPLOTS = True
# PRODUCT_CARBON_CONTENT_RELATIVE_ERROR = True
# PRODUCT_CARBON_CONTENT_RELATIVE_ERROR_PER_CATEGORY = True
# PRODUCT_CARBON_CONTENT_RATIO = True
# PRODUCT_CARBON_CONTENT_RATIO_PER_CATEGORY = True
# RELATIVE_DIFFERENCE_VS_RATIO = True
# RELATIVE_DIFFERENCE_VS_RELATIVE_ERROR = True
# RELATIVE_DIFFERENCE_CURVE = True
# RELATIVE_DIFFERENCE_DISTRIBUTION = True
RELATIVE_DIFFERENCE_DISTRIBUTION_PER_CATEGORY = True
# PRODUCT_CARBON_CONTENT_ESTIMATED_VS_THEORICAL = True
# RATIO_AND_RELATIVE_DIFFERENCE_DISTRIBUTION = True

sns.set(color_codes=True)
sns.set_style("whitegrid")

# Importing result data
carbon_balance_lci_filepath = os.path.join(RESULTS_FOLDER, SYSTEM_MODEL, 'carbon_balance_lci.csv')
carbon_balance_io_filepath = os.path.join(RESULTS_FOLDER, SYSTEM_MODEL, 'carbon_balance_io.csv')
coal_based_processes_filepath = os.path.join(RESULTS_FOLDER, SYSTEM_MODEL, 'coal_based_processes.csv')

if DATA_TYPE == 'S':
    ref_df = pd.read_csv(carbon_balance_lci_filepath, delimiter=';', encoding="latin1")
elif DATA_TYPE == 'U':
    ref_df = pd.read_csv(carbon_balance_io_filepath, delimiter=';', encoding="latin1")
else:
    ref_df = None

if PRODUCT_TYPE == 'Normal':
    ref_df = ref_df[ref_df['product'] >= 0]
elif PRODUCT_TYPE == 'Waste':
    ref_df = ref_df[ref_df['product'] < 0]
elif PRODUCT_TYPE == 'Total':
    pass
else:
    ref_df = None

coal_based_processes = list(pd.read_csv(coal_based_processes_filepath,
                                        delimiter=';', encoding="latin1", header=None)[0])

graphs_folder = os.path.join(BASE_FOLDER, '.graphs', 'article', SYSTEM_MODEL)

# Adjusting data types
ref_df.loc[:, 'isic_code_1': 'isic_code_4'] = ref_df.applymap(str).loc[:, 'isic_code_1': 'isic_code_4']

# Computing relative difference
ref_df.loc[:, 'relative_difference'] = abs((ref_df.total_output - ref_df.total_input) / \
                                           ((abs(ref_df.total_output) + abs(ref_df.total_input)) / 2))
ref_df.loc[ref_df.relative_difference.isnull(), 'relative_difference'] = 0

# Filtering carbonated products
carbonated_prod_df = ref_df[ref_df['product'] != 0]
non_carbonated_prod_df = ref_df[ref_df['product'] == 0]

# Calculating product carbon content relative error
carbonated_prod_df.loc[:, 'relative_error'] = abs(carbonated_prod_df.total_input - carbonated_prod_df.total_output) \
                                              / carbonated_prod_df['product']

# Calculating product carbon content ratio
carbonated_prod_df.loc[ref_df['product'] < 0, 'ratio'] = (carbonated_prod_df.technosphere_output +
                                                          carbonated_prod_df.biosphere_output -
                                                          carbonated_prod_df.total_input) \
                                                         / carbonated_prod_df['product']

carbonated_prod_df.loc[ref_df['product'] > 0, 'ratio'] = (carbonated_prod_df.total_input -
                                                          carbonated_prod_df.biosphere_output -
                                                          carbonated_prod_df.technosphere_output) \
                                                         / carbonated_prod_df['product']

# Computing log df
log_lci_df = ref_df.filter(items=['filename', 'total_input', 'total_output', 'isic_desc_1']
                           )[(ref_df['total_input'] != 0) & ref_df['total_output'] != 0]

log_lci_df.total_input = np.log10(log_lci_df.total_input)
log_lci_df.total_output = np.log10(log_lci_df.total_output)

COLORS = ['#0055a6', '#FE5745', '#7F7FCF', '#F39D01', '#8FB603', '#6C6C6Ccc', '#9E9E9Ecc', '#D0D0D0cc', '#ffffff']
MARKER_SIZE = 15

# # # # INPUT VS OUTPUT # # # #
if INPUT_VS_OUTPUT:
    # Computing correlation_index and p-value (on log data)
    correlation_index = np.corrcoef(x=log_lci_df['total_input'], y=log_lci_df['total_output'])[0][1]
    t, p_value = ttest_ind(log_lci_df['total_input'], log_lci_df['total_output'])

    # Getting data summary
    total_data = ref_df.shape[0]
    excluded_data = ref_df.shape[0] - log_lci_df.shape[0]

    fig, ax = plt.subplots(figsize=(6, 6))
    ax.set_xscale('log')
    ax.set_yscale('log')

    # Setting axes to the same size
    x_min, x_max = y_min, y_max = get_axes_limits(x=ref_df['total_input'], y=ref_df['total_output'])
    ax.set_xlim(x_min, x_max)
    ax.set_ylim(y_min, y_max)

    ax.scatter(x=ref_df['total_input'], y=ref_df['total_output'], marker='.', c=['#0055a6'], s=MARKER_SIZE, alpha=0.2)

    ax.annotate("Total: \nExcluded: \n"
                "R: \np-value: ",
                xy=(0.2, 0.97),
                xycoords='axes fraction',
                fontweight='bold',
                verticalalignment='top',
                horizontalalignment='right')

    ax.annotate("{:,}\n{:,}\n{:0.3f}\n{:0.3f}".format(total_data,
                                                      excluded_data,
                                                      correlation_index,
                                                      p_value).replace(',', ' '),
                xy=(0.2, 0.97),
                xycoords='axes fraction',
                fontweight='bold',
                verticalalignment='top',
                horizontalalignment='left')

    # Adding first bisector
    # abline(2, 0, COLORS[6])
    abline(10, 0, COLORS[6])
    abline(1, 0, COLORS[5])
    abline(0.1, 0, COLORS[6])
    # abline(0.5, 0, COLORS[6])

    # Adding lines equations
    ax.annotate('y = 10x', xy=(0.8, 0.97), xycoords='axes fraction', fontweight='bold', fontsize=11, rotation=45,
                color=COLORS[6])
    ax.annotate('y = x', xy=(0.89, 0.97), xycoords='axes fraction', fontweight='bold', fontsize=11, rotation=45,
                color=COLORS[5])
    ax.annotate('y = 0.1x', xy=(0.9, 0.89), xycoords='axes fraction', fontweight='bold', fontsize=11, rotation=45,
                color=COLORS[6])

    title = 'Carbon input vs output {} {}'.format(DATA_TYPE, PRODUCT_TYPE)

    plt.title(title)

    # Setting axes labels
    plt.xlabel('Input (kgC/FU)')
    plt.ylabel('Output (kgC/FU)')

    if EXPORT_IMAGE:
        filename = os.path.join(graphs_folder, title)
        plt.savefig(filename)
        plt.clf()
    else:
        plt.show()

if INPUT_VS_OUTPUT_PER_CATEGORY:

    for category in ref_df.isic_desc_1.unique():
        # Filtering values
        df = ref_df[ref_df.isic_desc_1 == category]

        if df.shape[0] == 0:
            continue

        log_df = log_lci_df[log_lci_df.isic_desc_1 == category]

        # Computing correlation_index and p-value (on log data)
        correlation_index = np.corrcoef(x=log_df['total_input'], y=log_df['total_output'])[0][1]
        t, p_value = ttest_ind(log_df['total_input'], log_df['total_output'])

        # Getting data summary
        total_data = df.shape[0]
        excluded_data = df.shape[0] - log_df.shape[0]

        # Plotting
        fig, ax = plt.subplots(figsize=(6, 6))
        ax.set_yscale('log')
        ax.set_xscale('log')

        # Setting axes to the same size
        x_min, x_max = y_min, y_max = get_axes_limits(x=df['total_input'], y=df['total_output'])
        ax.set_xlim(x_min, x_max)
        ax.set_ylim(y_min, y_max)

        # Adding first bisector
        abline(10, 0, COLORS[7])
        abline(2, 0, COLORS[6])
        abline(1, 0, COLORS[5])
        abline(0.5, 0, COLORS[6])
        abline(0.1, 0, COLORS[7])

        ax.scatter(x=df['total_input'], y=df['total_output'], marker='.', c=['#0055a6'], s=MARKER_SIZE, alpha=0.3)

        ax.annotate("Total: \nExcluded: \nR: \np-value: ",
                    xy=(0.2, 0.97),
                    xycoords='axes fraction',
                    fontweight='bold',
                    verticalalignment='top',
                    horizontalalignment='right')

        ax.annotate("{:,}\n{:,}\n{:0.3f}\n{:0.3f}".format(total_data,
                                                          excluded_data,
                                                          correlation_index,
                                                          p_value).replace(',', ' '),
                    xy=(0.2, 0.97),
                    xycoords='axes fraction',
                    fontweight='bold',
                    verticalalignment='top',
                    horizontalalignment='left')

        title = 'Carbon input vs output {} {}'.format(DATA_TYPE, PRODUCT_TYPE)
        plt.suptitle(title)
        plt.title(category, fontstyle='italic')

        # Setting axes labels
        plt.xlabel('Input (kgC/FU)')
        plt.ylabel('Output (kgC/FU)')

        plt.tight_layout()

        filename = os.path.join(graphs_folder, title, category)
        plt.savefig(filename)
        plt.clf()

if INPUT_VS_OUTPUT_ENERGY:

    # Filtering values
    df = ref_df[ref_df.isic_code_1 == 'D']

    df.loc[:, 'identify'] = 0
    df.loc[df.filename.isin(coal_based_processes), 'identify'] = 1

    log_df = df.filter(items=['filename', 'total_input', 'total_output', 'isic_desc_1', 'identify']
                       )[(df['total_input'] != 0) & df['total_output'] != 0]

    log_lci_df.total_input = np.log10(log_lci_df.total_input)
    log_lci_df.total_output = np.log10(log_lci_df.total_output)

    df = df.sort_values('identify')

    # Getting data summary
    total_data_coal = df[df['identify'] == 1].shape[0]
    excluded_data_coal = df[df['identify'] == 1].shape[0] - log_df[log_df['identify'] == 1].shape[0]
    total_data_other = df[df['identify'] == 0].shape[0]
    excluded_data_other = df[df['identify'] == 0].shape[0] - log_df[log_df['identify'] == 0].shape[0]

    # Computing correlation index for each group
    correlation_index_coal = np.corrcoef(x=log_df[log_df['identify'] == 1]['total_input'],
                                         y=log_df[log_df['identify'] == 1]['total_output'])[0][1]

    t_coal, p_value_coal = ttest_ind(log_df[log_df['identify'] == 1]['total_input'],
                                     log_df[log_df['identify'] == 1]['total_output'])

    correlation_index_other = np.corrcoef(x=log_df[log_df['identify'] == 0]['total_input'],
                                          y=log_df[log_df['identify'] == 0]['total_output'])[0][1]

    t_other, p_value_other = ttest_ind(log_df[log_df['identify'] == 0]['total_input'],
                                       log_df[log_df['identify'] == 0]['total_output'])

    # Plotting
    fig, ax = plt.subplots(figsize=(6, 6))

    ax.set_xscale('log')
    ax.set_yscale('log')

    # Setting axes to the same size
    x_min, x_max = y_min, y_max = get_axes_limits(x=df['total_input'], y=df['total_output'])
    ax.set_xlim(x_min, x_max)
    ax.set_ylim(y_min, y_max)

    df_0 = df[df['identify'] == 0]
    df_1 = df[df['identify'] == 1]
    ax.scatter(x=df_0['total_input'], y=df_0['total_output'], marker='.', c=COLORS[0], s=MARKER_SIZE, alpha=0.3)
    ax.scatter(x=df_1['total_input'], y=df_1['total_output'], marker='.', c=COLORS[1], s=MARKER_SIZE, alpha=0.3)

    ax.annotate("Coal-based",
                xy=(0.9, 0.20),
                color=COLORS[1],
                xycoords='axes fraction',
                fontweight='bold',
                verticalalignment='top',
                horizontalalignment='center')

    ax.annotate("\nTotal: \nExcluded: \nR: \np-value: ",
                xy=(0.9, 0.20),
                xycoords='axes fraction',
                fontweight='bold',
                verticalalignment='top',
                horizontalalignment='right')

    ax.annotate("\n{:,}\n{:,}\n{:0.3f}\n{:0.3f}".format(total_data_coal,
                                                        excluded_data_coal,
                                                        correlation_index_coal,
                                                        p_value_coal).replace(',', ' '),
                xy=(0.9, 0.20),
                xycoords='axes fraction',
                fontweight='bold',
                verticalalignment='top',
                horizontalalignment='left')

    ax.annotate("Non coal-based",
                xy=(0.2, 0.97),
                color=COLORS[0],
                xycoords='axes fraction',
                fontweight='bold',
                verticalalignment='top',
                horizontalalignment='center')

    ax.annotate("\nTotal: \nExcluded: \nR: \np-value: ",
                xy=(0.2, 0.97),
                xycoords='axes fraction',
                fontweight='bold',
                verticalalignment='top',
                horizontalalignment='right')

    ax.annotate("\n{:,}\n{:,}\n{:0.3f}\n{:0.3f}".format(total_data_other,
                                                        excluded_data_other,
                                                        correlation_index_other,
                                                        p_value_other).replace(',', ' '),
                xy=(0.2, 0.97),
                xycoords='axes fraction',
                fontweight='bold',
                verticalalignment='top',
                horizontalalignment='left')

    title = 'Carbon input vs output {} {}'.format(DATA_TYPE, PRODUCT_TYPE)

    plt.suptitle(title)
    plt.title('Electricity, gas, steam and air conditioning supply', fontstyle='italic')

    # Setting axes labels
    plt.xlabel('Input (kgC/FU)')
    plt.ylabel('Output (kgC/FU)')

    # Adding first bisector
    abline(10, 0, COLORS[7])
    abline(2, 0, COLORS[6])
    abline(1, 0, COLORS[5])
    abline(0.5, 0, COLORS[6])
    abline(0.1, 0, COLORS[7])

    # Adding lines equations
    ax.annotate('y = 10x', xy=(0.7, 0.97), xycoords='axes fraction', fontweight='bold', fontsize=11, rotation=45,
                color=COLORS[6])
    ax.annotate('y = 5x', xy=(0.83, 0.97), xycoords='axes fraction', fontweight='bold', fontsize=11, rotation=45,
                color=COLORS[6])
    ax.annotate('y = x', xy=(0.89, 0.965), xycoords='axes fraction', fontweight='bold', fontsize=11, rotation=45,
                color=COLORS[5])
    ax.annotate('y = 0.5x', xy=(0.89, 0.895), xycoords='axes fraction', fontweight='bold', fontsize=11, rotation=45,
                color=COLORS[6])
    ax.annotate('y = 0.1x', xy=(0.89, 0.775), xycoords='axes fraction', fontweight='bold', fontsize=11, rotation=45,
                color=COLORS[6])

    if EXPORT_IMAGE:
        filename = os.path.join(graphs_folder, 'Input Vs Output energy processes {} {}'.format(DATA_TYPE, PRODUCT_TYPE))
        plt.savefig(filename)
        plt.clf()
    else:
        plt.show()

if ENERGY_PROCESSES_RELATIVE_DIFFERENCE_HISTOGRAM:
    # Filtering values
    df = ref_df[ref_df.isic_code_1 == 'D']

    # Identifying processes
    df.loc[:, 'identify'] = 0
    df.loc[df.filename.isin(coal_based_processes), 'identify'] = 1

    # Plotting histograms
    fig = plt.figure()
    gs = gridspec.GridSpec(2, 1)

    ax1 = fig.add_subplot(gs[0])
    ax2 = fig.add_subplot(gs[1], sharex=ax1, sharey=ax1)

    ax1.hist(df[df['identify'] == 0]['relative_difference'], bins=20)
    ax2.hist(df[df['identify'] == 1]['relative_difference'], bins=20)

    ax1.set_title('Non coal based')
    ax2.set_title('Coal based')

    title = 'Energy processes relative difference {} {}'.format(DATA_TYPE, PRODUCT_TYPE)
    plt.title(title)

    plt.tight_layout()

    if EXPORT_IMAGE:
        filename = os.path.join(graphs_folder, title)
        plt.savefig(filename)
        plt.clf()
    else:
        plt.show()

if RELATIVE_DIFFERENCE_BOXPLOTS:
    isic_sub = list(ref_df.isic_desc_1.unique())
    isic_sub = [x for x in isic_sub if str(x) != 'nan']  # Remove nan

    # Sorting categories by number of data
    data_counts = [ref_df[ref_df['isic_desc_1'] == x].shape[0] for x in isic_sub]
    isic_sub = [x for _, x in sorted(zip(data_counts, isic_sub))]
    data_counts = sorted(data_counts)

    data = [ref_df[ref_df.isic_desc_1 == x]['relative_difference'] for x in isic_sub]

    labels = ['\n'.join(textwrap.wrap(str(l), 25)) for l in isic_sub]

    fig = plt.figure()
    gs = gridspec.GridSpec(2, 1, height_ratios=[1, 8])

    ax1 = fig.add_subplot(gs[0])
    ax2 = fig.add_subplot(gs[1], sharex=ax1)
    ax1.set_xlim(-0.1, 2.2)

    if RELATIVE_DIFFERENCE_BOXPLOT_X_IN_PROPORTION:
        ax1.set_xticks(
            [0, rel_diff(1, 1.25), rel_diff(1, 1.5), rel_diff(1, 2), rel_diff(1, 3), rel_diff(1, 5), rel_diff(1, 10),
             rel_diff(1, 50), 2])
        ax1.set_xticklabels(['1', '1.25', '1.5', '2', '3', '5', '10', '50', '∞'])
        ax2.set_xlabel('Proportion between C inputs and outputs (or inversly)')
    else:
        ax1.set_xticks([0, 1 / 3, 2 / 3, 1, 4 / 3, 5 / 3, 2])
        ax1.set_xticklabels(['0', '', '', '1', '', '', '2'])
        ax2.set_xlabel('Relative difference between C inputs and outputs')

    ax1.boxplot(ref_df['relative_difference'][ref_df['relative_difference'].notnull()], labels=['All categories'],
                vert=False, whis=[5, 95], showfliers=SHOW_BOXPLOT_OUTLIERS, widths=[0.35])
    title = 'Distribution of carbon input and output relative difference {} {}'.format(DATA_TYPE, PRODUCT_TYPE)
    ax1.set_title(title)

    total_data = ref_df.shape[0]
    ax1.text(2.17, 1 + 0.05, '{0:,}'.format(total_data).replace(',', ' '), horizontalalignment='right', size='small')

    ax2.boxplot(data, labels=labels, whis=[5, 95], vert=False, showfliers=SHOW_BOXPLOT_OUTLIERS)

    ax1.yaxis.grid(False)
    ax2.yaxis.grid(False)

    # Adding number of data for each category
    numBoxes = len(labels)
    pos = np.arange(numBoxes) + 1
    for tick in range(0, numBoxes):
        ax2.text(2.17, pos[tick] + 0.07, '{0:,}'.format(data_counts[tick]).replace(',', ' '),
                 horizontalalignment='right', size='small')

    height = 4 + len(data) * 0.4
    fig.set_size_inches(8, height)
    plt.tight_layout()

    if EXPORT_IMAGE:
        filename = os.path.join(graphs_folder, 'Carbon balance boxplots {} {}'.format(DATA_TYPE, PRODUCT_TYPE))
        plt.savefig(filename)
        plt.clf()
    else:
        plt.show()

if PRODUCT_CARBON_CONTENT_RELATIVE_ERROR:

    data = carbonated_prod_df.sort_values('relative_error').reset_index(drop=True)

    fig, ax = plt.subplots(figsize=(6, 6))
    ax.set_ylim(-1, 15)

    ax.scatter(x=100 * data.index / len(data), y=data.relative_error, marker='.', s=MARKER_SIZE)

    plt.xlabel('Rank (%)')
    plt.ylabel('Product carbon content relative error')

    ax.annotate("Total: {:,}\n".format(len(data)).replace(',', ' '),
                xy=(0.05, 0.97),
                xycoords='axes fraction',
                fontweight='bold',
                verticalalignment='top',
                horizontalalignment='left')

    title = 'Product carbon content relative error {} {}'.format(DATA_TYPE, PRODUCT_TYPE)
    plt.title(title)

    if EXPORT_IMAGE:
        filename = os.path.join(graphs_folder, title)
        plt.savefig(filename)
        plt.clf()
    else:
        plt.show()

if PRODUCT_CARBON_CONTENT_RELATIVE_ERROR_PER_CATEGORY:

    for category in carbonated_prod_df.isic_desc_1.unique():
        # Filtering values
        df = carbonated_prod_df[carbonated_prod_df.isic_desc_1 == category]

        if df.shape[0] == 0:
            continue

        data = df.sort_values('relative_error').reset_index(drop=True)

        fig, ax = plt.subplots(figsize=(6, 6))
        ax.set_ylim(-1, 15)

        plt.scatter(x=100 * data.index / len(data), y=data.relative_error, marker='.', s=MARKER_SIZE)

        plt.xlabel('Rank (%)')
        plt.ylabel('Product carbon content relative error')

        ax.annotate("Total: {:,}\n".format(len(data)).replace(',', ' '),
                    xy=(0.05, 0.97),
                    xycoords='axes fraction',
                    fontweight='bold',
                    verticalalignment='top',
                    horizontalalignment='left')

        title = 'Product carbon content relative error {} {}'.format(DATA_TYPE, PRODUCT_TYPE)
        plt.suptitle(title)
        plt.title(category, fontstyle='italic')

        filename = os.path.join(graphs_folder,
                                'Product carbon content relative error per category {} {}'.format(DATA_TYPE,
                                                                                                  PRODUCT_TYPE),
                                category)
        plt.savefig(filename)
        plt.clf()

if PRODUCT_CARBON_CONTENT_RATIO:

    data = carbonated_prod_df.sort_values('ratio').reset_index(drop=True)

    fig, ax = plt.subplots(figsize=(6, 6))
    ax.set_ylim(-5, 10)

    plt.scatter(x=100 * data.index / len(data), y=data.ratio, marker='.', s=MARKER_SIZE)

    abline(0, 1, COLORS[6])

    plt.xlabel('Rank (%)')
    plt.ylabel('Ratio between expected and calculated product carbon content')

    ax.annotate("Total: {:,}\n".format(len(data)).replace(',', ' '),
                xy=(0.05, 0.97),
                xycoords='axes fraction',
                fontweight='bold',
                verticalalignment='top',
                horizontalalignment='left')

    title = 'Product carbon content ratio {} {}'.format(DATA_TYPE, PRODUCT_TYPE)
    plt.title(title)

    if EXPORT_IMAGE:
        filename = os.path.join(graphs_folder, title)
        plt.savefig(filename)
        plt.clf()
    else:
        plt.show()

if PRODUCT_CARBON_CONTENT_RATIO_PER_CATEGORY:

    for category in carbonated_prod_df.isic_desc_1.unique():
        # Filtering values
        df = carbonated_prod_df[carbonated_prod_df.isic_desc_1 == category]

        if df.shape[0] == 0:
            continue

        data = df.sort_values('ratio').reset_index(drop=True)

        fig, ax = plt.subplots(figsize=(6, 6))
        ax.set_xlim(-1, 101)
        ax.set_ylim(-5, 10)

        plt.scatter(x=100 * data.index / len(data), y=data.ratio, marker='.', s=MARKER_SIZE)

        plt.xlabel('Rank (%)')
        plt.ylabel('Ratio between expected and calculated product carbon content')

        ax.annotate("Total: {:,}\n".format(len(data)).replace(',', ' '),
                    xy=(0.05, 0.97),
                    xycoords='axes fraction',
                    fontweight='bold',
                    verticalalignment='top',
                    horizontalalignment='left')

        title = 'Product carbon content ratio {} {}'.format(DATA_TYPE, PRODUCT_TYPE)
        plt.suptitle(title)
        plt.title(category, fontstyle='italic')

        filename = os.path.join(graphs_folder, 'Product carbon content ratio per category {} {}'.format(DATA_TYPE,
                                                                                                        PRODUCT_TYPE),
                                category)
        plt.savefig(filename)
        plt.clf()

if RELATIVE_DIFFERENCE_VS_RATIO:

    fig, ax = plt.subplots(figsize=(6, 6))
    ax.set_xlim(-5, 15)
    # ax.set_xscale('log')

    # Plotting the relative difference curve

    if PRODUCT_TYPE == 'Waste':
        a = list(np.linspace(-1, 1, 100)) + list(np.linspace(1, 15, 100))
        b = [abs(x / ((2 + x) / 2)) for x in a]
    else:
        a = list(np.linspace(0, 1, 100)) + list(np.linspace(1, 15, 100))
        b = [abs((x - 1) / ((1 + x) / 2)) for x in a]

    ax.plot(a, b, color=COLORS[4])

    # c = list(np.linspace(-4, 2, 100))
    # d = [abs((-(x-2) - 1) / ((1 - (x-2)) / 2)) for x in c]
    # ax.plot(c, d, color=COLORS[5])

    ax.scatter(x=carbonated_prod_df.ratio, y=carbonated_prod_df.relative_difference, marker='.', s=MARKER_SIZE,
               alpha=0.2)

    plt.ylabel('Total carbon input/output relative difference')
    plt.xlabel('Product carbon mass ratio')

    ax.annotate("Total: {:,}\n".format(len(carbonated_prod_df)).replace(',', ' '),
                xy=(0.05, 0.99),
                xycoords='axes fraction',
                fontweight='bold',
                verticalalignment='top',
                horizontalalignment='left')

    if PRODUCT_TYPE == 'Waste':
        equation = "y = relative difference(1, x+1)"
    else:
        equation = "y = relative difference(1, x)"

    ax.annotate(equation,
                xy=(0.505, 0.9),
                xycoords='axes fraction',
                fontweight='bold',
                verticalalignment='top',
                horizontalalignment='left',
                color=COLORS[4])

    title = 'Relative difference Vs Ratio {} {}'.format(DATA_TYPE, PRODUCT_TYPE)
    plt.title('Relative difference Vs Ratio ' + DATA_TYPE)

    if EXPORT_IMAGE:
        filename = os.path.join(graphs_folder, title)
        plt.savefig(filename)
        plt.clf()
    else:
        plt.show()

if RELATIVE_DIFFERENCE_VS_RELATIVE_ERROR:

    fig, ax = plt.subplots(figsize=(6, 6))
    ax.set_xlim(-5, 15)
    # ax.set_xscale('log')

    ax.scatter(x=carbonated_prod_df.relative_error, y=carbonated_prod_df.relative_difference, marker='.', s=MARKER_SIZE,
               alpha=0.2)

    plt.ylabel('Relative difference')
    plt.xlabel('Relative error')

    ax.annotate("Total: {:,}\n".format(len(carbonated_prod_df)).replace(',', ' '),
                xy=(0.05, 0.97),
                xycoords='axes fraction',
                fontweight='bold',
                verticalalignment='top',
                horizontalalignment='left')

    title = 'Relative difference Vs Relative error {} {}'.format(DATA_TYPE, PRODUCT_TYPE)
    plt.title(title)

    if EXPORT_IMAGE:
        filename = os.path.join(graphs_folder, title)
        plt.savefig(filename)
        plt.clf()
    else:
        plt.show()

if RELATIVE_DIFFERENCE_CURVE:
    a = list(np.linspace(0, 1, 100)) + list(np.linspace(1, 15, 100))
    b = [abs((x - 1) / ((1 + x) / 2)) for x in a]

    plt.plot(a, b)

    plt.ylabel('Relative difference')
    plt.xlabel('Ratio')

    if EXPORT_IMAGE:
        filename = os.path.join(graphs_folder, 'Relative difference curve')
        plt.savefig(filename)
        plt.clf()
    else:
        plt.show()

if RELATIVE_DIFFERENCE_DISTRIBUTION:

    data1 = ref_df.sort_values('relative_difference').reset_index(drop=True)
    data2 = carbonated_prod_df.sort_values('relative_difference').reset_index(drop=True)
    data3 = non_carbonated_prod_df.sort_values('relative_difference').reset_index(drop=True)

    fig, ax = plt.subplots(figsize=(6, 6))

    plt.scatter(x=100 * data1.index / len(data1), y=data1.relative_difference, marker='.', s=MARKER_SIZE,
                # color=COLORS[0]
                )
    # plt.scatter(x=100 * data2.index / len(data2), y=data2.relative_difference, marker='.', s=MARKER_SIZE,
    #             color=COLORS[1])
    # plt.scatter(x=100 * data3.index / len(data3), y=data3.relative_difference, marker='.', s=MARKER_SIZE,
    #             color=COLORS[2])

    plt.xlabel('Rank (%)')
    plt.ylabel('Relative difference between C input and output')

    ax.annotate(
        # "Total products\n"
        "Total: {:,}\n".format(len(data1)).replace(',', ' '),
        xy=(0.05, 0.94),
        xycoords='axes fraction',
        fontweight='bold',
        verticalalignment='top',
        horizontalalignment='left',
        # color=COLORS[0]
    )

    # ax.annotate("Carbonated products\nTotal: {:,}\n".format(len(data2)).replace(',', ' '),
    #             xy=(0.05, 0.885),
    #             xycoords='axes fraction',
    #             fontweight='bold',
    #             verticalalignment='top',
    #             horizontalalignment='left',
    #             color=COLORS[1])
    #
    # ax.annotate("Non-carbonated products\nTotal: {:,}\n".format(len(data3)).replace(',', ' '),
    #             xy=(0.05, 0.80),
    #             xycoords='axes fraction',
    #             fontweight='bold',
    #             verticalalignment='top',
    #             horizontalalignment='left',
    #             color=COLORS[2])

    # title = 'Relative difference {} {}'.format(DATA_TYPE, PRODUCT_TYPE)
    title = 'Relative difference distribution for Ecoinvent 3.4 processes'
    # plt.title(title)

    if EXPORT_IMAGE:
        filename = os.path.join(graphs_folder, title)
        plt.savefig(filename)
        plt.clf()
    else:
        plt.show()

if RELATIVE_DIFFERENCE_DISTRIBUTION_PER_CATEGORY:

    data = ref_df.sort_values('relative_difference').reset_index(drop=True)

    for category in data.isic_desc_1.unique():
        # Filtering values
        df = data[data.isic_desc_1 == category]
        df.reset_index(drop=True, inplace=True)
        df['rel_diff_rank'] = 100 * df.index / (len(df) - 1)

        if df.shape[0] == 0:
            continue
        fig, ax = plt.subplots(figsize=(7, 7))

        ax.plot(df.rel_diff_rank, df.relative_difference, color='lightgrey')
        ax.plot(df.rel_diff_rank, df.relative_difference, marker='.', markersize=MARKER_SIZE / 2, linestyle='')

        ax.set_xlabel('Rank (%)')
        ax.set_ylabel('Relative difference between C input and output')

        ax.annotate(
            "Total: {:,}\n".format(len(df)).replace(',', ' '),
            xy=(0.05, 0.94),
            xycoords='axes fraction',
            fontweight='bold',
            verticalalignment='top',
            horizontalalignment='left',
        )

        ax.set_ylim(-0.05, 2.05)

        title = 'Relative difference distribution\n' + category
        plt.title(title)

        plt.tight_layout()
        if EXPORT_IMAGE:
            filename = os.path.join(graphs_folder, 'Relative difference distribution', title.replace('\n', ' '))
            plt.savefig(filename)
            plt.clf()

        # Exporting distributions to be used in carbon_balance_interpretation.xlsx
        df[['relative_difference', 'rel_diff_rank']].to_csv(
            os.path.join(RESULTS_FOLDER, SYSTEM_MODEL, 'Relative difference distributions', category + '.csv'), sep=';')

if PRODUCT_CARBON_CONTENT_ESTIMATED_VS_THEORICAL:

    fig, ax = plt.subplots(figsize=(6, 6))
    # ax.set_xlim(-1, 1)
    # ax.set_ylim(-1, 1)
    # ax.set_xscale('log')
    # ax.set_yscale('log')

    ax.axis('equal')

    ax.scatter(x=np.log(carbonated_prod_df.total_input - carbonated_prod_df.biosphere_output),
               y=np.log(carbonated_prod_df.technosphere_output),
               marker='.', s=MARKER_SIZE, alpha=0.2)

    plt.xlabel('Theorical')
    plt.ylabel('Estimated')

    ax.annotate("Total: {:,}\n".format(len(carbonated_prod_df)).replace(',', ' '),
                xy=(0.05, 0.99),
                xycoords='axes fraction',
                fontweight='bold',
                verticalalignment='top',
                horizontalalignment='left')

    if EXPORT_IMAGE:
        filename = os.path.join(graphs_folder, 'Relative difference Vs Ratio ' + DATA_TYPE)
        plt.savefig(filename)
        plt.clf()
    else:
        plt.show()

if RATIO_AND_RELATIVE_DIFFERENCE_DISTRIBUTION:
    data1 = carbonated_prod_df.sort_values('ratio').reset_index(drop=True)
    data2 = carbonated_prod_df.sort_values('relative_difference').reset_index(drop=True)

    fig = plt.figure(figsize=(5, 6))
    gs = gridspec.GridSpec(2, 1)

    ax1 = fig.add_subplot(gs[0])
    ax2 = fig.add_subplot(gs[1], sharex=ax1)

    ax1.scatter(x=100 * data1.index / len(data1), y=data1.ratio, marker='.', s=MARKER_SIZE, color=COLORS[0])
    ax1.set_ylim(-5, 10)
    ax1.set_ylabel('Ratio between expected and\ncalculated product C content')

    ax2.scatter(x=100 * data2.index / len(data2), y=data2.relative_difference, marker='.', s=MARKER_SIZE,
                color=COLORS[0])
    ax2.set_ylim(-0.1, 2.1)
    ax2.set_ylabel('Relative difference between\nC input and output')

    plt.xlabel('Rank (%)')

    ax1.plot((90, 90), (-6, 11), '--', color=COLORS[5])
    ax2.plot((90, 90), (-1, 3), '--', color=COLORS[5])

    ax1.annotate("Total: {:,}\n".format(len(data1)).replace(',', ' '),
                 xy=(0.05, 0.97),
                 xycoords='axes fraction',
                 fontweight='bold',
                 verticalalignment='top',
                 horizontalalignment='left')

    plt.tight_layout()

    if EXPORT_IMAGE:
        filename = os.path.join(graphs_folder, 'Ratio and relative difference distribution')
        plt.savefig(filename)
        plt.clf()
    else:
        plt.show()

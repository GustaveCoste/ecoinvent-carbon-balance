import os

import untangle
import tqdm

from ecoinvent_carbon_balance.vars import ECOINVENT_IO_FOLDER
from ecoinvent_carbon_balance.lib import get_flow_carbon_content

results = dict()
results['shale'] = dict()
results['fish'] = dict()

for filename in tqdm.tqdm(os.listdir(ECOINVENT_IO_FOLDER)):
    filepath = os.path.join(ECOINVENT_IO_FOLDER, filename)

    process = untangle.parse(filepath)

    try:
        process = process.ecoSpold.childActivityDataset
    except AttributeError:
        process = process.ecoSpold.activityDataset

    if hasattr(process.flowData, 'elementaryExchange'):
        if type(process.flowData.elementaryExchange) == list:
            elementary_exchanges = process.flowData.elementaryExchange
        else:
            elementary_exchanges = [process.flowData.elementaryExchange, ]
    else:
        elementary_exchanges = []

    for exchange in elementary_exchanges:
        substance_name = exchange.name.cdata
        substance_carbon_content = get_flow_carbon_content(exchange)

        if not substance_carbon_content:
            continue

        # If the carbon content is not the same than the one(s) previously inserted for this substance,
        # collects the error
        if substance_name == 'Fish, pelagic, in ocean':
            if substance_carbon_content in results['fish']:
                results['fish'][substance_carbon_content] += 1
            else:
                results['fish'][substance_carbon_content] = 0

        if substance_name == 'Shale, in ground':
            if substance_carbon_content in results['shale']:
                results['shale'][substance_carbon_content] += 1
            else:
                results['shale'][substance_carbon_content] = 0

print(results)

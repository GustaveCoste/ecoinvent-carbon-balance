"""
Functions for ISIC/CPC codes management.
"""

import os
import csv
import re

from ecoinvent_carbon_balance.vars import FILES_FOLDER

data_filepath = os.path.join(FILES_FOLDER, 'isic_codes.csv')

# Reading data
isic_codes = dict()
with open(data_filepath, 'r') as file:
    reader = csv.DictReader(file, delimiter=';', quotechar='"')

    for row in reader:
        isic_codes[row['Code']] = row

isic_codes.pop('00')

cpc_conversion_dict = {'37440': '2394',
                       '17100': '3510',
                       '39149': '3821',  # Correspondance approximated by me as the CPC documentation does'nt give a correspondance for this code
                       '39270': '3821',  # Correspondance approximated by me as the CPC documentation does'nt give a correspondance for this code
                       '39363': '3821',  # Correspondance approximated by me as the CPC documentation does'nt give a correspondance for this code
                       '39920': '3700',  # Correspondance approximated by me as the CPC documentation does'nt give a correspondance for this code
                       '39240': '3821',  # Correspondance approximated by me as the CPC documentation does'nt give a correspondance for this code
                       '37111': '3821',  # Correspondance approximated by me as the CPC documentation does'nt give a correspondance for this code
                       '39': '3821',  # Correspondance approximated by me as the CPC documentation does'nt give a correspondance for this code
                       '34654': '149',
                       '39120': '3821',  # Correspondance approximated by me as the CPC documentation does'nt give a correspondance for this code
                       '39310': '3821',  # Correspondance approximated by me as the CPC documentation does'nt give a correspondance for this code
                       '39364': '3821',  # Correspondance approximated by me as the CPC documentation does'nt give a correspondance for this code
                       '39365': '3821',  # Correspondance approximated by me as the CPC documentation does'nt give a correspondance for this code
                       '39366': '3821',  # Correspondance approximated by me as the CPC documentation does'nt give a correspondance for this code
                       '39367': '3821',  # Correspondance approximated by me as the CPC documentation does'nt give a correspondance for this code
                       '39361': '3821',  # Correspondance approximated by me as the CPC documentation does'nt give a correspondance for this code
                       '3928': '3821',  # Correspondance approximated by me as the CPC documentation does'nt give a correspondance for this code
                       '39990': '3821',  # Correspondance approximated by me as the CPC documentation does'nt give a correspondance for this code
                       '1429': '729',
                       '17200': '3520',
                       '86119': '161',
                       '374': '2394',
                       '01913': '111',
                       '2151': '1010',
                       '21221': '1020'}


def get_isic_code(code, level, description=False):
    """
    From an ISIC code, returns the code/description corresponding the specified category.

    Args:
        code (str):
        level (int):
        description (bool):

    Returns:
        str:
    """

    # Removing level 5
    if re.fullmatch('\d*[a-z]', code):
        code = re.findall('\d+', code)[0]

    try:
        code_data = isic_codes[code]
    except KeyError:
        raise ValueError('The code does not exist.')

    while True:
        if int(code_data['Level']) < level:
            raise ValueError('The level of the code is higher than the asked level.')

        if int(code_data['Level']) > level:
            # Getting the parent code
            code_data = isic_codes[code_data['Parent']]

        if int(code_data['Level']) == level:
            result_code, result_description = code_data['Code'], code_data['Description']

            if description:
                return result_description
            else:
                return result_code


def cpc_to_isic(code):
    """
    Function for convertion from Central Product Classification to ISIC.

    Only works for some codes!

    Args:
        code (str):

    Returns:
        str
    """

    try:
        return cpc_conversion_dict[code]
    except KeyError:
        raise ValueError('This CPC code is unknown: {}'.format(code))

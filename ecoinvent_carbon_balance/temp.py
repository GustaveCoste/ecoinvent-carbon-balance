"""
Script to extract a list of every substances with their carbon content from Ecoinvent ecospolds
"""

import os
import cirpy
from urllib.error import URLError

import untangle
import tqdm

from ecoinvent_carbon_balance.vars import ECOINVENT_IO_FOLDER, RESULTS_FOLDER
from ecoinvent_carbon_balance.lib import get_flow_carbon_content


def formula_from_cas_number(cas):
    """
    Returns a formula from a CAS number by querying NCI Chemical Identifier Resolver (CIR)

    Args:
        cas (str): Cas number of the substance

    Returns:
        str: Chemical formula of the substance

    Examples:
        >>>formula_from_cas_number('50-99-7')
        'C6H12O6'
    """
    while True:
        if cas[0] == '0':
            cas = cas[1:]
        else:
            break

    if not cas:
        return None
    try:
        f = cirpy.resolve(cas, 'formula', resolvers=['cas_number'])
    except URLError:
        return None

    # If the above request failed, tries with every resolvers (slower but better results)
    if not f:
        try:
            f = cirpy.resolve(cas, 'formula')
        except URLError:
            return None

    return f


substances_filepath = os.path.join(RESULTS_FOLDER, 'substances.csv')
conflicts_filepath = os.path.join(RESULTS_FOLDER, 'conflicts.csv')

substances = dict()
conflicts = dict()

cas = set()

for filename in tqdm.tqdm(os.listdir(ECOINVENT_IO_FOLDER)):
    filepath = os.path.join(ECOINVENT_IO_FOLDER, filename)

    process = untangle.parse(filepath)

    try:
        process = process.ecoSpold.childActivityDataset
    except AttributeError:
        process = process.ecoSpold.activityDataset

    if hasattr(process.flowData, 'elementaryExchange'):
        if type(process.flowData.elementaryExchange) == list:
            elementary_exchanges = process.flowData.elementaryExchange
        else:
            elementary_exchanges = [process.flowData.elementaryExchange, ]
    else:
        elementary_exchanges = []

    for exchange in elementary_exchanges:
        substance_name = exchange.name.cdata
        substance_carbon_content = get_flow_carbon_content(exchange)

        if not substance_carbon_content:
            continue

        if exchange['casNumber']:
            cas.add(exchange['casNumber'])

i = 0
for sub in tqdm.tqdm(cas):
    if not formula_from_cas_number(sub):
        i += 1
    print(i)
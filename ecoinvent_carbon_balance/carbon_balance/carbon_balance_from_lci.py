"""
Script to calculates the carbon balance of each processes of the ecoinvent database on lci ecospold files
"""

import os
import csv

import untangle
import tqdm

from ecoinvent_carbon_balance.vars import ECOINVENT_LCI_FOLDER, RESULTS_FOLDER, SYSTEM_MODEL
from ecoinvent_carbon_balance.lib import get_tech_flow_carbon_mass, get_substances_carbon_content_dict
from ecoinvent_carbon_balance.isic_codes.isic_codes import get_isic_code, cpc_to_isic

result_filepath = os.path.join(RESULTS_FOLDER, SYSTEM_MODEL, 'carbon_balance_lci.csv')

# Reading substances
substances_carbon_content = get_substances_carbon_content_dict()

# Looping on every ecospold file
with open(result_filepath, 'w', newline='') as result:
    writer = csv.writer(result, delimiter=';')

    writer.writerow(
        ['filename', 'activity', 'geography', 'reference_product', 'amount', 'unit', 'isic_code_1', 'isic_code_2',
         'isic_code_3', 'isic_code_4', 'isic_desc_1', 'isic_desc_2', 'isic_desc_3', 'isic_desc_4', 'biosphere_input',
         'technosphere_input', 'total_input', 'biosphere_output', 'technosphere_output', 'total_output', 'product',
         'carbon_balance'])

    for filename in tqdm.tqdm(os.listdir(ECOINVENT_LCI_FOLDER)):

        # Extracting the process data
        filepath = os.path.join(ECOINVENT_LCI_FOLDER, filename)
        process = untangle.parse(filepath)
        try:
            process = process.ecoSpold.childActivityDataset
        except AttributeError:
            process = process.ecoSpold.activityDataset

        # Putting classifications in a list
        if hasattr(process.activityDescription, 'classification'):
            if type(process.activityDescription.classification) == list:
                classifications = process.activityDescription.classification
            else:
                classifications = [process.activityDescription.classification, ]
        else:
            classifications = []

        # Putting the elementary exchanges in a list
        if hasattr(process.flowData, 'elementaryExchange'):
            if type(process.flowData.elementaryExchange) == list:
                elementary_exchanges = process.flowData.elementaryExchange
            else:
                elementary_exchanges = [process.flowData.elementaryExchange, ]
        else:
            elementary_exchanges = []

        # Getting the product flow (LCI files has only one intermediate exchange: the product)
        assert process.flowData.intermediateExchange.outputGroup.cdata == '0' \
               and process.flowData.intermediateExchange['amount'] != '0'
        product_flow = process.flowData.intermediateExchange

        # Summing carbon input and outputs for biosphere and technosphere
        # Negative inputs are considered as positive outputs (and vice versa)
        bio_in = bio_out = tech_out = tech_in = prod_in = prod_out = 0

        for flow in elementary_exchanges:

            try:
                flow_carbon_content = substances_carbon_content[flow.name.cdata]
            except KeyError:
                continue

            # Input from biosphere
            if hasattr(flow, 'inputGroup'):
                if float(flow['amount']) > 0:
                    bio_in += flow_carbon_content * float(flow['amount'])
                else:
                    bio_out -= flow_carbon_content * float(flow['amount'])

            # Output to biosphere
            if hasattr(flow, 'outputGroup'):
                if float(flow['amount']) > 0:
                    bio_out += flow_carbon_content * float(flow['amount'])
                else:
                    bio_in -= flow_carbon_content * float(flow['amount'])

        # Product flow carbon mass
        product_carbon_mass = get_tech_flow_carbon_mass(product_flow)
        if product_carbon_mass > 0:
            prod_out = product_carbon_mass
        else:
            prod_in = abs(product_carbon_mass)

        carbon_balance = bio_in + tech_in - bio_out - tech_out

        # Getting the classification
        isic = None
        for classif in classifications:
            if classif.classificationSystem.cdata == 'ISIC rev.4 ecoinvent':
                isic = classif.classificationValue.cdata

        if isic is None:
            try:
                cpc = [x for x in product_flow.classification
                       if x.classificationSystem.cdata == 'CPC'][0].classificationValue.cdata
                cpc = cpc.split(':')[0]
                isic = cpc_to_isic(cpc)
            except IndexError:
                # Handling of one particular case
                if process.activityDescription.activity.activityName.cdata == \
                        'log, energy wood, split, measured as solid wood under bark, Recycled Content cut-off':
                    isic = '220: Logging'

        if isic is not None:
            isic_code = isic.split(':')[0]

            codes = dict()
            descriptions = dict()
            for lvl in range(1, 5):
                try:
                    codes[lvl] = get_isic_code(isic_code, lvl)
                    descriptions[lvl] = get_isic_code(isic_code, lvl, description=True)
                except ValueError:
                    codes[lvl] = ''
                    descriptions[lvl] = ''
        else:
            raise Exception('No ISIC code.')

        writer.writerow([filename,
                         process.activityDescription.activity.activityName.cdata,
                         process.activityDescription.geography.shortname.cdata,
                         product_flow.name.cdata,
                         product_flow['amount'],
                         product_flow.unitName.cdata,
                         codes[1],
                         codes[2],
                         codes[3],
                         codes[4],
                         descriptions[1],
                         descriptions[2],
                         descriptions[3],
                         descriptions[4],
                         str(bio_in),
                         str(tech_in),
                         str(bio_in + tech_in + prod_in),
                         str(bio_out),
                         str(tech_out),
                         str(bio_out + tech_out + prod_out),
                         str(product_carbon_mass),
                         str(carbon_balance)])
